import jp.ac.nitech.cs.seki.algorithm.bide.CachedLSSetBIDE;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.impl.CachedSequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.DefaultSequenceDatabase;
import jp.ac.nitech.cs.seki.evaluator.EvaluatorRunner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class Main {
    private static final List<List<Object>> EVALUATION_PARAMETERS = new ArrayList<List<Object>>() {
        {
            add(Arrays.asList(0.1, 2, 0.5));

            /*/ D5C20N10R0.5 parameters
            add(Arrays.asList(0.028, 1, 0.5));
            add(Arrays.asList(0.026, 1, 0.5));
            add(Arrays.asList(0.024, 1, 0.5));
            add(Arrays.asList(0.022, 1, 0.5));
            add(Arrays.asList(0.020, 1, 0.5));

            add(Arrays.asList(0.024, 1, 0.6));
            add(Arrays.asList(0.024, 1, 0.7));
            add(Arrays.asList(0.024, 1, 0.8));
            add(Arrays.asList(0.024, 1, 0.9));
            //*/

            /*/ D10C10N10R0.5 parameters
            add(Arrays.asList(0.009, 1, 0.5));
            add(Arrays.asList(0.008, 1, 0.5));
            add(Arrays.asList(0.007, 1, 0.5));
            add(Arrays.asList(0.006, 1, 0.5));
            add(Arrays.asList(0.005, 1, 0.5));

            add(Arrays.asList(0.005, 1, 0.6));
            add(Arrays.asList(0.005, 1, 0.7));
            add(Arrays.asList(0.005, 1, 0.8));
            add(Arrays.asList(0.005, 1, 0.9));
            //*/

            /*/ Gazelle parameters
            add(Arrays.asList(0.00108, 1, 0.5));
            add(Arrays.asList(0.00104, 1, 0.5));
            add(Arrays.asList(0.00100, 1, 0.5));
            add(Arrays.asList(0.00096, 1, 0.5));
            add(Arrays.asList(0.00092, 1, 0.5));

            add(Arrays.asList(0.00092, 1, 0.6));
            add(Arrays.asList(0.00092, 1, 0.7));
            add(Arrays.asList(0.00092, 1, 0.8));
            add(Arrays.asList(0.00092, 1, 0.9));
            //*/

            /*/ TotInfo parameters
            add(Arrays.asList(0.078, 1, 0.5));
            add(Arrays.asList(0.073, 1, 0.5));
            add(Arrays.asList(0.067, 1, 0.5));
            add(Arrays.asList(0.062, 1, 0.5));
            add(Arrays.asList(0.056, 1, 0.5));

            add(Arrays.asList(0.056, 1, 0.6));
            add(Arrays.asList(0.056, 1, 0.7));
            add(Arrays.asList(0.056, 1, 0.8));
            add(Arrays.asList(0.056, 1, 0.9));
            //*/

            /*/ FIFA parameters
            add(Arrays.asList(0.24, 1, 0.5));
            add(Arrays.asList(0.22, 1, 0.5));
            add(Arrays.asList(0.20, 1, 0.5));
            add(Arrays.asList(0.18, 1, 0.5));
            add(Arrays.asList(0.16, 1, 0.5));

            add(Arrays.asList(0.16, 1, 0.6));
            add(Arrays.asList(0.16, 1, 0.7));
            add(Arrays.asList(0.16, 1, 0.8));
            add(Arrays.asList(0.16, 1, 0.9));
            //*/
        }
    };

    private static final List<List<Object>> CACHED_EVALUATION_PARAMETERS = new ArrayList<List<Object>>() {
        {
            add(Arrays.asList(0.1, 2, 0.5, CachedLSSetBIDE.getInstance()));

            /*/ D5C20N10R0.5 parameters
            add(Arrays.asList(0.028, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.026, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.024, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.022, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.020, 1, 0.5, CachedLSSetBIDE.getInstance()));

            add(Arrays.asList(0.024, 1, 0.6, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.024, 1, 0.7, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.024, 1, 0.8, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.024, 1, 0.9, CachedLSSetBIDE.getInstance()));
            //*/

            /*/ D10C10N10R0.5 parameters
            add(Arrays.asList(0.009, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.008, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.007, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.006, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.005, 1, 0.5, CachedLSSetBIDE.getInstance()));

            add(Arrays.asList(0.005, 1, 0.6, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.005, 1, 0.7, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.005, 1, 0.8, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.005, 1, 0.9, CachedLSSetBIDE.getInstance()));
            //*/

            /*/ Gazelle parameters
            add(Arrays.asList(0.00108, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.00104, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.00100, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.00096, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.00092, 1, 0.5, CachedLSSetBIDE.getInstance()));

            add(Arrays.asList(0.00092, 1, 0.6, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.00092, 1, 0.7, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.00092, 1, 0.8, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.00092, 1, 0.9, CachedLSSetBIDE.getInstance()));
            //*/

            /*/ TotInfo parameters
            add(Arrays.asList(0.078, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.073, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.067, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.062, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.056, 1, 0.5, CachedLSSetBIDE.getInstance()));

            add(Arrays.asList(0.056, 1, 0.6, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.056, 1, 0.7, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.056, 1, 0.8, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.056, 1, 0.9, CachedLSSetBIDE.getInstance()));
            //*/

            /*/ FIFA parameters
            add(Arrays.asList(0.24, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.22, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.20, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.18, 1, 0.5, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.16, 1, 0.5, CachedLSSetBIDE.getInstance()));

            add(Arrays.asList(0.16, 1, 0.6, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.16, 1, 0.7, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.16, 1, 0.8, CachedLSSetBIDE.getInstance()));
            add(Arrays.asList(0.16, 1, 0.9, CachedLSSetBIDE.getInstance()));
            //*/
        }
    };

    private static String getDatabaseName(String filePath) {
        File file = new File(filePath);
        String filename = file.getName();
        return filename.substring(0, filename.lastIndexOf('.'));
    }

    public static void main(String[] args) {
        String databaseFilePath = "";

        // databaseFilePath = "resources/dataset/sdb.txt";
        // databaseFilePath = "resources/dataset/gazelle.txt";
        // databaseFilePath = "resources/dataset/gazelle2.txt";
        // databaseFilePath = "resources/dataset/D5C20N10R0.5.txt";
        // databaseFilePath = "resources/dataset/D10C10N10R0.5.txt";
        // databaseFilePath = "resources/dataset/D5C20N10S20.txt";
        // databaseFilePath = "resources/dataset/totinfo-int.txt";
        // databaseFilePath = "resources/dataset/FIFA.txt";

        final String databaseName = getDatabaseName(databaseFilePath);

        {
            CachedSequenceDatabase<Integer> cachedDatabase = CachedSequenceDatabase.newIntegerLinkedListInstance(databaseFilePath, Sequence::newArrayListInstance);
            EvaluatorRunner<Integer> cachedRunner;

            cachedRunner = new EvaluatorRunner<>(false, false, true, () -> new SimpleDateFormat("yyMMdd.HHmmss.SSS").format(new Date().getTime()) + "-" + databaseName + "-Cached.log");
            cachedRunner.useNR3ParallelLoopFused(false);
            cachedRunner.useNR3Pipeline(false);
            cachedRunner.useNR3Naive(false);
            cachedRunner.useNR3Sequential(false);
            cachedRunner.useNR3SequentialLoopFused(false);

            cachedRunner.run(cachedDatabase, CACHED_EVALUATION_PARAMETERS);
        }

        {
            DefaultSequenceDatabase<Integer> database = DefaultSequenceDatabase.newIntegerLinkedListInstance(databaseFilePath, Sequence::newArrayListInstance);
            EvaluatorRunner<Integer> runner;

            runner = new EvaluatorRunner<>(false, false, true, () -> new SimpleDateFormat("yyMMdd.HHmmss.SSS").format(new Date().getTime()) + "-" + databaseName + "-Default.log");
            runner.useNR3Enhanced(false);
            runner.useNR3ParallelLoopFused(false);
            runner.useNR3Pipeline(false);
            runner.useNR3Naive(false);
            runner.useNR3Sequential(false);
            runner.useNR3SequentialLoopFused(false);
            runner.useBOBSequential(false);
            runner.useEBISequential(false);

            runner.run(database, EVALUATION_PARAMETERS);
        }
    }
}

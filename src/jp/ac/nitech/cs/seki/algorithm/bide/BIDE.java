package jp.ac.nitech.cs.seki.algorithm.bide;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;

import java.util.List;
import java.util.function.Consumer;

public interface BIDE {
    String getName();

    /**
     * specified sequence database should use distinct record ids;
     *
     * @param sequenceDatabase
     * @param minSup
     * @param emitSequence
     */
    <T> void run(SequenceDatabase<T> sequenceDatabase, int minSup, Consumer<Sequence<T>> emitSequence);

    <T> boolean canPruneByBackScan(SequenceDatabase<T> projected, Sequence<T> prefix);

    <T> List<List<T>> getSemiMaximumPeriod(SequenceDatabaseRecord<T> record, Sequence<T> prefix);
}

package jp.ac.nitech.cs.seki.algorithm.bide;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.CachedSequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.PartialSequence;

import java.util.*;
import java.util.function.Consumer;

public class CachedLSSetBIDE implements BIDE {
    public final static String NAME = "CachedLSSetBIDE";
    private final static CachedLSSetBIDE instance = new CachedLSSetBIDE();

    private CachedLSSetBIDE() {
    }

    public static CachedLSSetBIDE getInstance() {
        return instance;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public <T> void run(SequenceDatabase<T> sequenceDatabase, int minSup, Consumer<Sequence<T>> emitSequence) {
        bide(sequenceDatabase, Sequence.newArrayListInstance(), minSup, emitSequence);
    }

    protected <T> void bide(SequenceDatabase<T> optimizedDatabase, Sequence<T> sequence, int minSup, Consumer<Sequence<T>> emitSequence) {
        List<T> frequentItems = optimizedDatabase.getFrequentItems(minSup);

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(item);

            SequenceDatabase<T> projected = optimizedDatabase.getProjected(item);

            if (canPruneByBackScan(projected, prefix)) {
                continue;
            }
            // skip forward/backward extension check for LS-Set
            emitSequence.accept(prefix);
            bide(projected, prefix, minSup, emitSequence);
        }
    }

    @Override
    public <T> boolean canPruneByBackScan(SequenceDatabase<T> projected, Sequence<T> prefix) {
        boolean included = false;
        List<List<T>> semiMaxPeriodItems = new ArrayList<>(prefix.size());
        int previousRecordID = -1;
        for (SequenceDatabaseRecord<T> record : projected) {
            if (record.getID() == previousRecordID) {
                continue;
            }
            previousRecordID = record.getID();
            List<List<T>> semiMaxPeriod = getSemiMaximumPeriod(record, prefix);

            boolean isEmpty = true;
            if (included) {
                Iterator<List<T>> semiMaxPeriodIterator = semiMaxPeriod.iterator();
                for (List<T> items : semiMaxPeriodItems) {
                    items.retainAll(semiMaxPeriodIterator.next());
                    isEmpty &= items.isEmpty();
                }
            } else {
                included = true;
                for (List<T> items : semiMaxPeriod) {
                    semiMaxPeriodItems.add(new ArrayList<>(items));
                    isEmpty &= items.isEmpty();
                }
            }
            if (isEmpty) {
                return false;
            }
        }
        return true;
    }

    @Override
    public <T> List<List<T>> getSemiMaximumPeriod(SequenceDatabaseRecord<T> cachedRecord, Sequence<T> prefix) {
        Sequence<T> sequence = ((PartialSequence<T>) cachedRecord.getSequence()).getOriginalSequence();
        Map<T, int[]> posMap = ((CachedSequenceDatabaseRecord<T>) cachedRecord).getPosMap();
        int offSet = ((CachedSequenceDatabaseRecord<T>) cachedRecord).getParentOffset();

        int prefixSize = prefix.size();

        // get first instances of pattern P in S
        int[] firstInstancePosition = new int[prefixSize];
        int instanceLastIndex = 0;
        int y_pre = offSet - 1;
        int i = 0;
        Map<T, Integer> prevPosMap = new HashMap<>();
        for (T value : prefix) {
            // find next_y in pos_set s.t. next_y > y_pre
            int[] posSet = posMap.get(value);
            int prevPos = prevPosMap.getOrDefault(value, posSet.length);
            int y = 0;
            for (; y < prevPos; ++y) {
                if (posSet[y] <= y_pre) {
                    prevPosMap.put(value, y - 1);
                    firstInstancePosition[i] = posSet[y - 1];
                    y_pre = firstInstancePosition[i];
                    ++i;
                    break;
                }
            }
            if (y == prevPos) {
                prevPosMap.put(value, y - 1);
                firstInstancePosition[i] = posSet[y - 1];
                y_pre = firstInstancePosition[i];
                ++i;
            }
        }
        instanceLastIndex = y_pre;  // prefixの(sequenceにおける)最初の出現の最後のindex (初期のsequenceにおけるposition)

        // get LFi
        int[] lastInFirstPosition = new int[prefixSize];
        int currentPosition = instanceLastIndex;
        for (i = prefixSize - 1; i >= 0; --i) { // prefixを後ろからチェックしてゆく
            T event = prefix.get(i);
            int[] posSet = posMap.get(event);
            int y = prevPosMap.get(event);
            for (; y >= 0; --y) {
                if (posSet[y] == currentPosition) {
                    prevPosMap.put(event, y + 1);
                    lastInFirstPosition[i] = posSet[y];
                    currentPosition = lastInFirstPosition[i] - 1;
                    break;
                } else if (posSet[y] > currentPosition) {
                    prevPosMap.put(event, y + 2);
                    lastInFirstPosition[i] = posSet[y + 1];
                    currentPosition = lastInFirstPosition[i] - 1;
                    break;
                }
            }
            if (y == -1) {
                prevPosMap.put(event, y + 2);
                lastInFirstPosition[i] = posSet[y + 1];
                currentPosition = lastInFirstPosition[i] - 1;
            }
        }

        // check semi-maximum period
        List<List<T>> semiMaxPeriod = new ArrayList<>(prefixSize);
        semiMaxPeriod.add(sequence.subList(0, lastInFirstPosition[0] - offSet));
        for (i = 1; i < prefixSize; ++i) {
            semiMaxPeriod.add(sequence.subList(firstInstancePosition[i - 1] - offSet + 1, lastInFirstPosition[i] - offSet));
        }

        return semiMaxPeriod;
    }
}

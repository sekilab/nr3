package jp.ac.nitech.cs.seki.algorithm.bide;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.PartialSequence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class LSSetBIDE implements BIDE {
    public final static String NAME = "LSSetBIDE";
    private final static LSSetBIDE instance = new LSSetBIDE();

    private LSSetBIDE() {
    }

    public static BIDE getInstance() {
        return instance;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public <T> void run(SequenceDatabase<T> sequenceDatabase, int minSup, Consumer<Sequence<T>> emitSequence) {
        bide(sequenceDatabase, Sequence.newArrayListInstance(), minSup, emitSequence);
    }

    protected <T> void bide(SequenceDatabase<T> optimizedDatabase, Sequence<T> sequence, int minSup, Consumer<Sequence<T>> emitSequence) {
        List<T> frequentItems = optimizedDatabase.getFrequentItems(minSup);

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(item);

            SequenceDatabase<T> projected = optimizedDatabase.getProjected(item);

            if (canPruneByBackScan(projected, prefix)) {
                continue;
            }
            // skip forward/backward extension check for LS-Set
            emitSequence.accept(prefix);
            bide(projected, prefix, minSup, emitSequence);
        }
    }

    @Override
    public <T> boolean canPruneByBackScan(SequenceDatabase<T> projected, Sequence<T> prefix) {
        boolean included = false;
        List<List<T>> semiMaxPeriodItems = new ArrayList<>(prefix.size());
        int previousRecordID = -1;
        for (SequenceDatabaseRecord<T> record : projected) {
            if (record.getID() == previousRecordID) {
                continue;
            }
            previousRecordID = record.getID();
            List<List<T>> semiMaxPeriod = getSemiMaximumPeriod(record, prefix);

            boolean isEmpty = true;
            if (included) {
                Iterator<List<T>> semiMaxPeriodIterator = semiMaxPeriod.iterator();
                for (List<T> items : semiMaxPeriodItems) {
                    items.retainAll(semiMaxPeriodIterator.next());
                    isEmpty &= items.isEmpty();
                }
            } else {
                included = true;
                for (List<T> partialSequence : semiMaxPeriod) {
                    semiMaxPeriodItems.add(new ArrayList<>(partialSequence));
                    isEmpty &= partialSequence.isEmpty();
                }
            }
            if (isEmpty) {
                return false;
            }
        }

        return true;
    }

    @Override
    public <T> List<List<T>> getSemiMaximumPeriod(SequenceDatabaseRecord<T> record, Sequence<T> prefix) {
        // Definition A.2 i-th last-in-first appearance
        // Given S containing P=<e1...en>, LFi is the last appearance of ei in the first instance of pattern P in S
        // (1) i = n, last(Sp)
        // (2) 1 <= i < n, while LFi must appear before LFi+1
        // e.g.) S=CAABC, P=CAC, LF2 = 2nd A in S

        // Definition A.3 i-th semi-maximum period
        // Given S containing P, it is the piece of sequence
        // (1) 1 < i <= n, between the end of the first instance of pattern <e1...ei-1> in S (exclusive) and LFi (exclusive)
        // (2) i = 1, before the LF1
        // e.g.) S=ABCB, P=AC, 2nd-SMP = B, 1st-SMP = (empty)

        // get first instances of pattern P in S
        Sequence<T> sequence = ((PartialSequence<T>) record.getSequence()).getOriginalSequence();

        int prefixSize = prefix.size();
        int sequenceSize = sequence.size();

        List<Integer> firstInstancePosition = new ArrayList<>(prefixSize);
        int instanceLastIndex = 0;
        for (T event : prefix) {
            for (; instanceLastIndex < sequenceSize; ++instanceLastIndex) {
                if (event.equals(sequence.get(instanceLastIndex))) {
                    firstInstancePosition.add(instanceLastIndex++);
                    break;
                }
            }
        }
        --instanceLastIndex;

        // get LFi
        int[] lastInFirstPosition = new int[prefixSize];
        int currentPosition = instanceLastIndex;
        for (int i = prefixSize - 1; i >= 0; --i) {
            T event = prefix.get(i);
            for (; currentPosition >= 0; --currentPosition) {
                if (event.equals(sequence.get(currentPosition))) {
                    lastInFirstPosition[i] = currentPosition--;
                    break;
                }
            }
        }

        // check semi-maximum period
        List<List<T>> semiMaxPeriod = new ArrayList<>(prefixSize);
        Iterator<Integer> firstInstancePositionIterator = firstInstancePosition.iterator();
        semiMaxPeriod.add(sequence.subList(0, lastInFirstPosition[0]));
        for (int i = 1; i < prefixSize; ++i) {
            semiMaxPeriod.add(sequence.subList(firstInstancePositionIterator.next() + 1, lastInFirstPosition[i]));
        }

        return semiMaxPeriod;
    }
}

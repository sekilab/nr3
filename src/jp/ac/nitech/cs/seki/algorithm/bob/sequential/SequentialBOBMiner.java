package jp.ac.nitech.cs.seki.algorithm.bob.sequential;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.bob.AbstractBOBMiner;
import jp.ac.nitech.cs.seki.algorithm.bob.BackwardGrowTask;
import jp.ac.nitech.cs.seki.algorithm.bob.ForwardGrowTask;
import jp.ac.nitech.cs.seki.algorithm.bob.RuleFormingTask;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SuffixPatternTree;
import jp.ac.nitech.cs.seki.datastructure.impl.ConcurrentSuffixPatternTree;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SequentialBOBMiner<T> extends AbstractBOBMiner<T> {
    public SequentialBOBMiner(String name) {
        super(name);
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, BOBParameter parameter) {
        return mine(seqDB, parameter, new EvaluationLogger(getName(), parameter));
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> sequenceDatabase, BOBParameter parameter, EvaluationLogger logger) {
        sequenceDatabase = sequenceDatabase.getOptimized(parameter.getMinSup());

        logger.logTimestamp("SeqDB Optimized");

        Collection<ForwardGrowTask.PrefixPattern<T>> prefixPatterns = new LinkedList<>();
        ForwardGrowTask<T> forwardGrowTask = new ForwardGrowTask<>(sequenceDatabase, parameter, prefixPatterns::add, logger);
        forwardGrowTask.run();
        logger.logTimestamp("ForwardGrow Done");

        SuffixPatternTree<T> postTree = new ConcurrentSuffixPatternTree<>(null, null, null);
        BackwardGrowTask<T> backwardGrowTask = new BackwardGrowTask<>(sequenceDatabase, parameter, postTree, logger);
        backwardGrowTask.run();

        logger.logTimestamp("BackwardGrowDone");

        Collection<NR3Rule<T>> ruleCandidates = new ConcurrentLinkedQueue<>();
        RuleFormingTask<T> ruleFormingTask = new RuleFormingTask<>(sequenceDatabase, prefixPatterns, postTree, parameter, ruleCandidates::add, logger);
        ruleFormingTask.run();

        logger.logTimestamp("RuleForming Done");
        logger.addMetadata("ruleCandidatesSize", ruleCandidates.size());

        List<Rule<T>> result = Miner.removeRedundantRules(ruleCandidates);

        logger.logTimestamp("RedunRuleRemoved");

        return result;
    }
}

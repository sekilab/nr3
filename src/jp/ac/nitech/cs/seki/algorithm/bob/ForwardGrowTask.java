package jp.ac.nitech.cs.seki.algorithm.bob;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.List;
import java.util.function.Consumer;

/**
 * Theorem 2. (Pruning Redundant Pre-Conds)
 * R'pre > Rpre
 * (i) R'pre = P1 ++ e ++ P2, Rpre = P1 ++ P2 (some e, nonempty P1, P2)
 * (ii) SeqDB_Rpre = SeqDB_R'pre
 * (Rpre ++ P) -> post is redundant
 * <p>
 * Use LS-Set BIDE
 *
 * @param <T>
 */
public class ForwardGrowTask<T> implements Runnable {
    private final SequenceDatabase<T> sequenceDatabase;
    private final BOBMiner.BOBParameter parameter;
    private final Consumer<PrefixPattern<T>> printPrefix;

    private final EvaluationLogger logger;
    private int prefixCount;

    public ForwardGrowTask(SequenceDatabase<T> sequenceDatabase, BOBMiner.BOBParameter parameter, Consumer<PrefixPattern<T>> printPrefix, EvaluationLogger logger) {
        this.parameter = parameter;
        this.sequenceDatabase = sequenceDatabase;
        this.printPrefix = printPrefix;

        this.logger = logger;
    }

    @Override
    public void run() {
        forwardGrow(sequenceDatabase, Sequence.newArrayListInstance());
        logger.addMetadata("PreCondSize", prefixCount);
    }

    private void forwardGrow(SequenceDatabase<T> optimizedDatabase, Sequence<T> sequence) {
        List<T> frequentItems = optimizedDatabase.getFrequentItems(parameter.getMinSup());

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(item);

            SequenceDatabase<T> projected = optimizedDatabase.getProjected(item);

            if (parameter.getBIDE().canPruneByBackScan(projected, prefix)) {
                continue;
            }

            ++prefixCount;
            printPrefix.accept(new PrefixPattern<>(prefix, projected.getAllProjectedWithThis(item, false).size()));
            forwardGrow(projected, prefix);
        }
    }

    public static class PrefixPattern<T> {
        private final Sequence<T> prefix;
        private final int prefixInstanceSupport;

        public PrefixPattern(Sequence<T> prefix, int prefixInstanceSupport) {
            this.prefix = prefix;
            this.prefixInstanceSupport = prefixInstanceSupport;
        }

        public Sequence<T> getPrefix() {
            return prefix;
        }

        public int getPrefixInstanceSupport() {
            return prefixInstanceSupport;
        }
    }
}

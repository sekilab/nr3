package jp.ac.nitech.cs.seki.algorithm.bob;

public abstract class AbstractBOBMiner<T> implements BOBMiner<T> {
    private final String name;

    protected AbstractBOBMiner(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}

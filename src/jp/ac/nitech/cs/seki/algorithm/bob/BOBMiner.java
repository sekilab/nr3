package jp.ac.nitech.cs.seki.algorithm.bob;

import jp.ac.nitech.cs.seki.algorithm.EvaluationParameter;
import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.bide.BIDE;
import jp.ac.nitech.cs.seki.algorithm.bide.LSSetBIDE;

public interface BOBMiner<T> extends Miner<T, BOBMiner.BOBParameter> {
    class BOBParameter implements EvaluationParameter {
        private final int minSup;
        private final int minSupAll;
        private final double minConf;
        private final BIDE bide;

        public BOBParameter() {
            this(200, 1, 0.5, null);
        }

        public BOBParameter(int minSup, int minSupAll, double minConf) {
            this(minSup, minSupAll, minConf, null);
        }

        public BOBParameter(int minSup, int minSupAll, double minConf, BIDE bide) {
            this.minSup = minSup;
            this.minSupAll = minSupAll;
            this.minConf = minConf;
            this.bide = bide == null ? LSSetBIDE.getInstance() : bide;
        }

        @Override
        public int getMinSup() {
            return minSup;
        }

        @Override
        public int getMinSupAll() {
            return minSupAll;
        }

        @Override
        public double getMinConf() {
            return minConf;
        }

        public BIDE getBIDE() {
            return bide;
        }

        @Override
        public String toString() {
            return "{min_sup: " + minSup + ", min_sup-all: " + minSupAll + ", min_conf: " + String.format("%.1f%%", minConf * 100) + ", " + bide.getName() + "}";
        }
    }

    @Override
    default BOBParameter getParameter(int databaseSize, Object... params) {
        return new BOBParameter(
                (int) Math.round(((double) params[0]) * databaseSize),
                (int) params[1],
                (double) params[2],
                params.length >= 4 ? (BIDE) params[3] : LSSetBIDE.getInstance()
        );
    }
}

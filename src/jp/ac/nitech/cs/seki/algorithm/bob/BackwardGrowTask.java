package jp.ac.nitech.cs.seki.algorithm.bob;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.SuffixPatternTree;
import jp.ac.nitech.cs.seki.datastructure.impl.PartialSequence;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

/**
 * Lemma1. R'post > Rpost
 * (i) R'post = P1 ++ e ++ P2, Rpost = P1 ++ P2 (some e, P1, nonempty P2)
 * (ii) SeqDB_pre_Rpost = SeqDB_pre_R'post
 * pre -> (P ++ Rpost) is not confidence-closed (i.e., there exists another R' > R, conf(R) = conf(R'))
 * <p>
 * Lemma2.
 * (i) R'post = P1 ++ e ++ P2, Rpost = P1 ++ P2 (some e, P1, nonempty P2)
 * (iii) all j: SeqDB_suf-j_Rpost = Seq_suf-j_R'post
 * (iv) all j: |(SeqDB_suf-j_Rpost)all_Rpost| = |(SeqDB_suf-j_R'post)all_R'post|
 * pre -> (P ++ Rpost) is not support-closed (i.e., there exists another R' > R, sup(R) = sup(R') and sup_all(R) = sup_all(R')
 * <p>
 * Theorem3. (Pruning Redundant Post-Conds)
 * Rpost satisfying Lemma 1, 2
 * pre -> (P ++ Rpost) is redundant
 * <p>
 * Use modified LS-Set BIDE
 * - Use LS-Set BIDE for Lemma1
 * - Implement Lemma2
 *
 * @param <T>
 */
public class BackwardGrowTask<T> implements Runnable {
    private final SequenceDatabase<T> sequenceDatabase;
    private final BOBMiner.BOBParameter parameter;
    private final SuffixPatternTree<T> suffixPatternTree;

    private final EvaluationLogger logger;
    private int suffixCount;

    public BackwardGrowTask(SequenceDatabase<T> sequenceDatabase, BOBMiner.BOBParameter parameter, SuffixPatternTree<T> suffixPatternTree, EvaluationLogger logger) {
        this.parameter = parameter;
        this.suffixPatternTree = suffixPatternTree;
        this.sequenceDatabase = sequenceDatabase;

        this.logger = logger;
    }

    @Override
    public void run() {
        backwardGrow(sequenceDatabase, Sequence.newArrayListInstance(), suffixPatternTree);
        logger.addMetadata("PostCondSize", suffixCount);
    }

    private void backwardGrow(SequenceDatabase<T> optimizedDatabase, Sequence<T> sequence, SuffixPatternTree<T> node) {
        List<T> frequentItems = optimizedDatabase.getFrequentItems(parameter.getMinSup());

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(0, item);

            SequenceDatabase<T> projected = optimizedDatabase.getPrefixProjected(item);

            if (!canPruneByForwardScan(projected, prefix)) {
                SuffixPatternTree<T> child = node.addChild(item, projected);
                ++suffixCount;
                backwardGrow(projected, prefix, child);
            }
        }
    }

    private boolean canPruneByForwardScan(SequenceDatabase<T> projected, Sequence<T> prefix) {
        // Lemma 1. BackScan (BIDE)
        Optional<List<List<T>>> confidenceNotClosedEventResult = getConfidenceNotClosedEvents(projected, prefix);
        if (!confidenceNotClosedEventResult.isPresent()) {
            return false;
        }

        // Lemma 2
        //  prefix := P1 ++ P2
        //  Rpost := P1 ++ P2
        //  R'post := P1 ++ e ++ P2
        List<List<T>> semiMaxPeriodItems = confidenceNotClosedEventResult.get();
        for (int i = 0; i < semiMaxPeriodItems.size(); ++i) {
            for (T item : semiMaxPeriodItems.get(i)) {
                // R'post = prefix(1..i-1) + item + prefix(i...n)
                Sequence<T> candidate = Sequence.newArrayListInstance(prefix);
                candidate.add(i + 1, item);

                if (!isSupportClosed(projected, prefix, candidate)) {
                    return true;
                }
            }
        }
        return false;
    }

    // whether prefix is already support-closed so that candidate can not prune prefix
    private boolean isSupportClosed(SequenceDatabase<T> projected, Sequence<T> prefix, Sequence<T> candidate) {
        for (SequenceDatabaseRecord<T> record : projected) {
            PartialSequence<T> sequence = ((PartialSequence<T>) record.getSequence());
            Sequence<T> parent = sequence.getOriginalSequence();

            // Satisfied condition in this record
            //  - SeqDB-suf-1_Rpost = SeqDB-suf-1_R'post (confidence not-closed events)
            //  - first(Rpost)= first(R'post) (confidence not-closed events)

            // check sup-all(SeqDB-suf-1_Rpost,Rpost) == sup-all(SeqDB-suf-1_R'post,R'post)
            List<Integer> firstPrefixIndices = getFirstPrefixIndices(sequence, prefix.get(0));

            int[] prefixInstances = getInstances(parent, firstPrefixIndices, prefix);
            int[] candidateInstances = getInstances(parent, firstPrefixIndices, candidate);

            for (int i = 0; i < prefixInstances.length; ++i) {
                if (prefixInstances[i] != candidateInstances[i]) {
                    return true;
                }
            }
        }
        return false;
    }

    private int[] getInstances(Sequence<T> parent, List<Integer> firstPrefixIndices, Sequence<T> prefix) {
        int prefixSize = prefix.size();
        int firstPrefixIndexSize = firstPrefixIndices.size();
        int[] prefixPosition = new int[firstPrefixIndexSize];
        int[] sufInstances = new int[firstPrefixIndexSize];
        for (int currentSufIndex = firstPrefixIndexSize - 1; currentSufIndex >= 0; --currentSufIndex) {
            int parentFrom = firstPrefixIndices.get(firstPrefixIndexSize - currentSufIndex - 1);
            int parentTo = currentSufIndex > 0 ? firstPrefixIndices.get(firstPrefixIndexSize - currentSufIndex) - 1 : parent.size() - 1;
            for (int parentIndex = parentFrom; parentIndex <= parentTo; ++parentIndex) {
                T event = parent.get(parentIndex);
                for (int sufIndex = currentSufIndex; sufIndex < firstPrefixIndexSize; ++sufIndex) {
                    if (event.equals(prefix.get(prefixPosition[sufIndex]))) {
                        if (prefixPosition[sufIndex] == prefixSize - 1) {
                            ++sufInstances[sufIndex];
                        } else {
                            ++prefixPosition[sufIndex];
                        }
                    }
                }
            }
        }
        return sufInstances;
    }

    private List<Integer> getFirstPrefixIndices(Sequence<T> sequence, T event) {
        List<Integer> indices = new ArrayList<>();
        int sequenceSize = sequence.size();
        for (int i = 0; i < sequenceSize; ++i) {
            if (event.equals(sequence.get(i))) {
                indices.add(i);
            }
        }
        indices.add(sequenceSize);
        return indices;
    }

    private Optional<List<List<T>>> getConfidenceNotClosedEvents(SequenceDatabase<T> projected, Sequence<T> prefix) {
        boolean included = false;
        List<List<T>> semiMaxPeriodItems = new ArrayList<>(prefix.size());
        for (SequenceDatabaseRecord<T> record : projected) {
            List<List<T>> semiMaxPeriod = getSemiMaximumPeriod(record, prefix);

            boolean isEmpty = true;
            if (included) {
                Iterator<List<T>> semiMaxPeriodIterator = semiMaxPeriod.iterator();
                for (List<T> items : semiMaxPeriodItems) {
                    items.retainAll(semiMaxPeriodIterator.next());
                    isEmpty &= items.isEmpty();
                }
            } else {
                included = true;
                for (List<T> partialSequence : semiMaxPeriod) {
                    semiMaxPeriodItems.add(new ArrayList<>(partialSequence));
                    isEmpty &= partialSequence.isEmpty();
                }
            }
            if (isEmpty) {
                return Optional.empty();
            }
        }
        return Optional.of(semiMaxPeriodItems);
    }

    private List<List<T>> getSemiMaximumPeriod(SequenceDatabaseRecord<T> record, Sequence<T> prefix) {
        // Definition A.2 i-th last-in-first appearance
        // Given S containing P=<e1...en>, LFi is the last appearance of ei in the first instance of pattern P in S
        // (1) i = n, last(Sp)
        // (2) 1 <= i < n, while LFi must appear before LFi+1
        // e.g.) S=CAABC, P=CAC, LF2 = 2nd A in S

        // Definition A.3 i-th semi-maximum period
        // Given S containing P, it is the piece of sequence
        // (1) 1 < i <= n, between the end of the first instance of pattern <e1...ei-1> in S (exclusive) and LFi (exclusive)
        // (2) i = 1, before the LF1
        // e.g.) S=ABCB, P=AC, 2nd-SMP = B, 1st-SMP = (empty)

        // get first instances of pattern P in S
        Sequence<T> sequence = ((PartialSequence<T>) record.getSequence()).getOriginalSequence();

        int prefixSize = prefix.size();
        int sequenceSize = sequence.size();

        int[] firstInstancePosition = new int[prefixSize];
        int firstInstancePositionIndex = prefixSize - 1;
        int instanceLastIndex = sequenceSize - 1;
        for (int i = prefixSize - 1; i >= 0; --i) {
            T event = prefix.get(i);
            for (; instanceLastIndex >= 0; --instanceLastIndex) {
                if (event.equals(sequence.get(instanceLastIndex))) {
                    firstInstancePosition[firstInstancePositionIndex--] = instanceLastIndex--;
                    break;
                }
            }
        }
        ++instanceLastIndex;

        // get LFi
        int[] lastInFirstPosition = new int[prefixSize];
        int lastInFirstPositionIndex = 0;
        int currentPosition = instanceLastIndex;
        for (T event : prefix) {
            for (; currentPosition < sequenceSize; ++currentPosition) {
                if (event.equals(sequence.get(currentPosition))) {
                    lastInFirstPosition[lastInFirstPositionIndex++] = currentPosition++;
                    break;
                }
            }
        }

        // check semi-maximum period
        List<List<T>> semiMaxPeriod = new ArrayList<>(prefixSize);
        for (int i = 0; i < prefixSize - 1; ++i) {
            semiMaxPeriod.add(sequence.subList(lastInFirstPosition[i] + 1, firstInstancePosition[i + 1]));
        }
        semiMaxPeriod.add(sequence.subList(lastInFirstPosition[prefixSize - 1] + 1, sequenceSize));

        return semiMaxPeriod;
    }
}

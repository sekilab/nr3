package jp.ac.nitech.cs.seki.algorithm.bob;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SuffixPatternTree;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public class RuleFormingTask<T> implements Runnable {
    private final SequenceDatabase<T> sequenceDatabase;
    private final SuffixPatternTree<T> postTree;
    private final BOBMiner.BOBParameter parameter;
    private final Consumer<NR3Rule<T>> printRule;
    private final EvaluationLogger logger;

    private final Map<Sequence<T>, ForwardGrowTask.PrefixPattern<T>> prefixPatternMap;
    private int totalRuleCheckCount;
    private int totalInstanceSupportCount;
    private int totalRuleCount;

    public RuleFormingTask(SequenceDatabase<T> sequenceDatabase, Collection<ForwardGrowTask.PrefixPattern<T>> prefixPatterns, SuffixPatternTree<T> postTree, BOBMiner.BOBParameter parameter, Consumer<NR3Rule<T>> printRule, EvaluationLogger logger) {
        this.sequenceDatabase = sequenceDatabase;
        this.postTree = postTree;
        this.parameter = parameter;
        this.printRule = printRule;
        this.logger = logger;

        this.prefixPatternMap = new HashMap<>();
        for (ForwardGrowTask.PrefixPattern<T> prefixPattern : prefixPatterns) {
            prefixPatternMap.put(prefixPattern.getPrefix(), prefixPattern);
        }
    }

    @Override
    public void run() {
        int effectivePrefixCount = 0;
        int effectiveRuleCheckCount = 0;
        for (ForwardGrowTask.PrefixPattern<T> pattern : prefixPatternMap.values()) {
            int prevRuleCount = totalRuleCount;
            int prevRuleCheckCount = totalRuleCheckCount;
            traverseSuffix(postTree, Sequence.newArrayListInstance(), pattern);
            if (totalRuleCount != prevRuleCount) {
                ++effectivePrefixCount;
                effectiveRuleCheckCount += (totalRuleCheckCount - prevRuleCheckCount);
            }
        }
        logger.addMetadata("Effective Prefix Count", effectivePrefixCount);
        logger.addMetadata("Effective Rule Check Count", effectiveRuleCheckCount);
        logger.addMetadata("Total Instance Support Count", totalInstanceSupportCount);
        logger.addMetadata("Total Rule Check Count", totalRuleCheckCount);
    }

    private void traverseSuffix(SuffixPatternTree<T> parent, Sequence<T> post, ForwardGrowTask.PrefixPattern<T> prefixPattern) {
        Sequence<T> prefix = prefixPattern.getPrefix();
        int prefixInstanceSupport = prefixPattern.getPrefixInstanceSupport();

        for (SuffixPatternTree<T> postNode : parent.getChildren()) {
            ++totalRuleCheckCount;

            Sequence<T> sequence = Sequence.newArrayListInstance(post);
            sequence.add(0, postNode.getValue());

            int sequenceSupport = postNode.getProjected().getProjected(prefix).size();
            if (sequenceSupport < parameter.getMinSup()) {
                continue;
            }

            double confidence = (double) postNode.getProjected().getAllProjected(prefix, false).size() / prefixInstanceSupport;
            if (confidence < parameter.getMinConf()) {
                continue;
            }

            int instanceSupport = getInstanceSupport(prefix, sequence);
            if (instanceSupport < parameter.getMinSupAll()) {
                continue;
            }

            NR3Rule<T> rule = new NR3Rule<>(Sequence.newArrayListInstance(), prefix, sequence, sequenceSupport, -1, instanceSupport, confidence);
            printRule.accept(rule);
            ++totalRuleCount;

            traverseSuffix(postNode, sequence, prefixPattern);
        }
    }

    private int getInstanceSupport(Sequence<T> prefix, Sequence<T> post) {
        ++totalInstanceSupportCount;
        Sequence<T> rule = Sequence.newArrayListInstance(prefix);
        rule.addAll(post);
        return sequenceDatabase.getAllProjected(rule, false).size();
    }
}

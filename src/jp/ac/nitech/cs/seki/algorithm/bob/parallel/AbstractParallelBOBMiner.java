package jp.ac.nitech.cs.seki.algorithm.bob.parallel;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.bob.AbstractBOBMiner;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.datastructure.parallel.pool.ThreadPoolGenerator;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public abstract class AbstractParallelBOBMiner<T> extends AbstractBOBMiner<T> {
    private final ThreadPoolGenerator threadPoolGenerator;

    public AbstractParallelBOBMiner(String name, ThreadPoolGenerator threadPoolGenerator) {
        super(name);
        this.threadPoolGenerator = threadPoolGenerator;
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, BOBParameter parameter) {
        return mine(seqDB, parameter, new EvaluationLogger(getName(), parameter));
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, BOBParameter parameter, EvaluationLogger logger) {
        ExecutorService threadPool = threadPoolGenerator.generate();

        Future<List<NR3Rule<T>>> future = threadPool.submit(() -> {
            SequenceDatabase<T> optimizedSequenceDatabase = seqDB.getOptimized(parameter.getMinSup());
            logger.logTimestamp("SeqDB Optimized");
            return getBOBRules(threadPool, logger, optimizedSequenceDatabase, parameter);
        });

        List<NR3Rule<T>> rules = null;
        try {
            rules = future.get();
            logger.logTimestamp("MainThread done");
            threadPool.shutdown();
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Something wrong occurred while parallel working.");
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return Miner.removeRedundantRules(rules);
    }

    protected abstract List<NR3Rule<T>> getBOBRules(ExecutorService threadPool, EvaluationLogger logger, SequenceDatabase<T> sequenceDatabase, BOBParameter parameter) throws ExecutionException, InterruptedException;
}

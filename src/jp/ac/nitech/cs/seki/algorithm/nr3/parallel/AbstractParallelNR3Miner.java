package jp.ac.nitech.cs.seki.algorithm.nr3.parallel;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.AbstractNR3Miner;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.datastructure.parallel.pool.ThreadPoolGenerator;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;

public abstract class AbstractParallelNR3Miner<T> extends AbstractNR3Miner<T> {
    private final ThreadPoolGenerator threadPoolGenerator;

    public AbstractParallelNR3Miner(String name, ThreadPoolGenerator threadPoolGenerator) {
        super(name);
        this.threadPoolGenerator = threadPoolGenerator;
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter) {
        return mine(seqDB, parameter, new EvaluationLogger(getName(), parameter));
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter, EvaluationLogger logger) {
        ExecutorService threadPool = threadPoolGenerator.generate();

        Collection<NR3Rule<T>> ruleCandidates = new ConcurrentLinkedQueue<>();
        Future future = threadPool.submit(() -> {
            SequenceDatabase<T> optimizedSeqDB = seqDB.getOptimized(parameter.getMinSup());
            getNR3Rules(threadPool, logger, optimizedSeqDB, parameter, ruleCandidates::add);
        });

        try {
            future.get();
            threadPool.shutdown();
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Something wrong occurred while parallel working.");
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        logger.logTimestamp("Rule generated");
        logger.addMetadata("RuleCand Size", ruleCandidates.size());

        List<Rule<T>> result = Miner.removeRedundantRules(ruleCandidates);

        logger.logTimestamp("Redun. removed");

        return result;
    }

    protected abstract void getNR3Rules(ExecutorService threadPool, EvaluationLogger logger, SequenceDatabase<T> seqDB, NR3Parameter parameter, Consumer<NR3Rule<T>> printRule);
}

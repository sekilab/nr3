package jp.ac.nitech.cs.seki.algorithm.nr3;

import jp.ac.nitech.cs.seki.algorithm.ebi.BackwardGrowTask;
import jp.ac.nitech.cs.seki.algorithm.ebi.EBIMiner;
import jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialLoopFusedNR3RuleGenerator;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.datastructure.impl.PartialSequence;
import jp.ac.nitech.cs.seki.datastructure.impl.PositionRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.SuffixPositionTree;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class EnhancedNR3PostMiningTask<T> implements Runnable {
    private final SequenceDatabase<T> projectedDatabase;
    private final Sequence<T> prefix;
    private final NR3Miner.NR3Parameter parameter;
    private final Consumer<NR3Rule<T>> printRule;
    private final SequentialLoopFusedNR3RuleGenerator.RuleGenerationLog<T> log;
    private final EvaluationLogger logger;

    public EnhancedNR3PostMiningTask(SequenceDatabase<T> projectedDatabase, Sequence<T> prefix, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> printRule, SequentialLoopFusedNR3RuleGenerator.RuleGenerationLog<T> log, EvaluationLogger logger) {
        this.projectedDatabase = projectedDatabase;
        this.prefix = prefix;
        this.parameter = parameter;
        this.printRule = printRule;
        this.log = log;
        this.logger = logger;
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();

        SuffixPositionTree<T> suffixPositionTree = new SuffixPositionTree<>(null, null, null);

        BackwardGrowTask<T> backwardGrowTask = new BackwardGrowTask<>(projectedDatabase, new EBIMiner.EBIParameter(parameter.getMinSup(), parameter.getMinSupAll(), parameter.getMinConf()), suffixPositionTree, logger);
        backwardGrowTask.run();

        traverseSuffix(suffixPositionTree, Sequence.newArrayListInstance());

        log.addExecutionTime(startTime, System.currentTimeMillis());
    }

    private void traverseSuffix(SuffixPositionTree<T> parent, Sequence<T> post) {
        for (SuffixPositionTree<T> postNode : parent.getChildren()) {
            Sequence<T> sequence = Sequence.newArrayListInstance(post);
            sequence.add(0, postNode.getValue());

            SupportConfidencePair supportConfidencePair = getSupportAndConfidence(prefix.get(prefix.size() - 1), sequence, projectedDatabase, postNode.getPositionRecords());

            if (supportConfidencePair.sequenceSupport < parameter.getMinSup() ||
                    supportConfidencePair.confidence < parameter.getMinConf() ||
                    supportConfidencePair.instanceSupport < parameter.getMinSupAll()) {
                continue;
            }

            NR3Rule<T> rule = new NR3Rule<>(Sequence.newArrayListInstance(), prefix, sequence, supportConfidencePair.sequenceSupport, projectedDatabase.size(), supportConfidencePair.instanceSupport, supportConfidencePair.confidence);
            printRule.accept(rule);

            traverseSuffix(postNode, sequence);
        }
    }

    private SupportConfidencePair getSupportAndConfidence(T prefixLastEvent, Sequence<T> post, SequenceDatabase<T> projected, List<PositionRecord> positionRecords) {
        int sup = 0;
        int postPreAll = 0;
        int preAll = 0;
        int supAll = 0;

        List<Integer> support = new ArrayList<>();

        Iterator<SequenceDatabaseRecord<T>> recordIterator = projected.iterator();
        Iterator<PositionRecord> postIterator = positionRecords.iterator();
        SequenceDatabaseRecord<T> record = null;
        PositionRecord postRecord = null;
        while((record != null || recordIterator.hasNext()) && (postRecord != null || postIterator.hasNext())) {
            if (record == null) {
                record = recordIterator.next();
            }
            if (postRecord == null) {
                postRecord = postIterator.next();
            }
            if (record.getID() > postRecord.getID()) {
                postRecord = null;
                continue;
            }

            PartialSequence<T> partialSequence = (PartialSequence<T>) record.getSequence();
            Sequence<T> sequence = partialSequence.getOriginalSequence();
            int preLastIndex = partialSequence.getBeginIndex() - 1;
            int sequenceSize = sequence.size();

            if (record.getID() < postRecord.getID()) {
                // confidence
                for (int i = preLastIndex; i < sequenceSize; ++i) {
                    if (prefixLastEvent.equals(sequence.get(i))) {
                        support.add(record.getID());
                        ++preAll;
                    }
                }
                record = null;
                continue;
            }

            int postFirstIndex = postRecord.getPosition(); // index of first(post)

            int recordID = record.getID();

            record = null;
            postRecord = null;

            // confidence
            for (int i = preLastIndex; i < sequenceSize; ++i) {
                if (prefixLastEvent.equals(sequence.get(i))) {
                    ++preAll;
                    if (i < postFirstIndex) {
                        ++postPreAll;
                    }
                    support.add(recordID);
                }
            }

            // sequence support
            if (postFirstIndex <= preLastIndex) {
                continue;
            }
            ++sup;

            // instance support;
            int cur = preLastIndex + 1;
            for (T event : post) {
                for (; cur < sequenceSize; ++cur) {
                    if (event.equals(sequence.get(cur))) {
                        break;
                    }
                }
                if (cur == sequenceSize) {
                    break;
                }
            }
            if (cur != sequenceSize) {
                T lastPostEvent = post.get(post.size() - 1);
                for (; cur < sequenceSize; ++cur) {
                    if (lastPostEvent.equals(sequence.get(cur))) {
                        ++supAll;
                    }
                }
            }
        }
        while (record != null || recordIterator.hasNext()) {
            if (record == null) {
                record = recordIterator.next();
            }
            PartialSequence<T> partialSequence = (PartialSequence<T>) record.getSequence();
            Sequence<T> sequence = partialSequence.getOriginalSequence();
            int preLastIndex = partialSequence.getBeginIndex() - 1;
            int sequenceSize = sequence.size();
            for (int i = preLastIndex; i < sequenceSize; ++i) {
                if (prefixLastEvent.equals(sequence.get(i))) {
                    support.add(record.getID());
                    ++preAll;
                }
            }
            record = null;
        }
        return new SupportConfidencePair(sup, supAll, (double) postPreAll / preAll);
    }

    private class SupportConfidencePair {
        int sequenceSupport;
        int instanceSupport;
        double confidence;
        SupportConfidencePair(int sequenceSupport, int instanceSupport, double confidence) {
            this.sequenceSupport = sequenceSupport;
            this.instanceSupport = instanceSupport;
            this.confidence = confidence;
        }
    }
}

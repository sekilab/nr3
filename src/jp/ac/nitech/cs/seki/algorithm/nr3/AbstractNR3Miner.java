package jp.ac.nitech.cs.seki.algorithm.nr3;

import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.List;

public abstract class AbstractNR3Miner<T> implements NR3Miner<T> {
    private final String name;

    protected AbstractNR3Miner(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter) {
        return mine(seqDB, parameter, new EvaluationLogger(getName(), parameter));
    }
}

package jp.ac.nitech.cs.seki.algorithm.nr3.sequential;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.AbstractNR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.LoopFusedNR3RuleGenerator;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SequentialLoopFusedNR3Miner<T> extends AbstractNR3Miner<T> {
    private final LoopFusedNR3RuleGenerator ruleGenerator;

    public SequentialLoopFusedNR3Miner(String name, LoopFusedNR3RuleGenerator ruleGenerator) {
        super(name);
        this.ruleGenerator = ruleGenerator;
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter) {
        return mine(seqDB, parameter, new EvaluationLogger(getName(), parameter));
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter, EvaluationLogger logger) {
        seqDB = seqDB.getOptimized(parameter.getMinSup());
        logger.addMetadata("DB Size", seqDB.size());
        logger.addMetadata("DB Avg.SeqLen", seqDB.stream().collect(Collectors.averagingLong(record -> record.getSequence().size())));
        logger.addMetadata("DB Unique Items", seqDB.getFrequentItems(parameter.getMinSup()).size());
//        if (seqDB instanceof CachedSequenceDatabase) {
//            logger.addMetadata("DB Avg.Table Event", seqDB.stream().collect(Collectors.averagingLong(record -> ((CachedSequenceDatabaseRecord<T>) record).getPosMap().keySet().size())));
//            logger.addMetadata("DB Avg.Table Length", seqDB.stream().collect(Collectors.averagingDouble(record -> ((CachedSequenceDatabaseRecord<T>) record).getPosMap().values().stream().collect(Collectors.averagingLong(arr -> arr.length)))));
//        }
        logger.logTimestamp("DB Optimized");

        List<NR3Rule<T>> ruleCandidates = new LinkedList<>();
        ruleGenerator.generate(seqDB, parameter, ruleCandidates::add, logger);

        logger.logTimestamp("Rule generated");
        logger.addMetadata("RuleCand Size", ruleCandidates.size());

        List<Rule<T>> result = Miner.removeRedundantRules(ruleCandidates);
        logger.logTimestamp("Redun. removed");

        logger.addMetadata("Rule Avg.Len", result.stream().collect(Collectors.averagingLong(r -> r.getPreCondition().size() + r.getPostCondition().size())));

        return result;
    }
}

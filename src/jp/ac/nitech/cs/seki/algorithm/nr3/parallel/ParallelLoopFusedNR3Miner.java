package jp.ac.nitech.cs.seki.algorithm.nr3.parallel;

import jp.ac.nitech.cs.seki.algorithm.nr3.LoopFusedNR3PostMiningTask;
import jp.ac.nitech.cs.seki.algorithm.nr3.LoopFusedNR3RuleGenerator;
import jp.ac.nitech.cs.seki.algorithm.nr3.NR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialLoopFusedNR3RuleGenerator;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.datastructure.parallel.pool.ThreadPoolGenerator;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class ParallelLoopFusedNR3Miner<T> extends AbstractParallelNR3Miner<T> {
    public ParallelLoopFusedNR3Miner(String name, ThreadPoolGenerator threadPoolGenerator) {
        super(name, threadPoolGenerator);
    }

    @Override
    protected void getNR3Rules(ExecutorService threadPool, EvaluationLogger logger, SequenceDatabase<T> seqDB, NR3Parameter parameter, Consumer<NR3Rule<T>> printRule) {
        LoopFusedNR3RuleGenerator.RuleGenerationLog<T> log = new LoopFusedNR3RuleGenerator.RuleGenerationLog<>();
        logger.addMetadata("Rule Generation Log", log);
        bide(threadPool, seqDB, Sequence.newArrayListInstance(), parameter, printRule, log);

        logger.logTimestamp("BIDE finished");
    }

    private static <T> void bide(ExecutorService threadPool, SequenceDatabase<T> optimizedDatabase, Sequence<T> sequence, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> emitRule, SequentialLoopFusedNR3RuleGenerator.RuleGenerationLog<T> log) {
        List<T> frequentItems = optimizedDatabase.getFrequentItems(parameter.getMinSup());

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(item);

            SequenceDatabase<T> projected = optimizedDatabase.getProjected(item);
            log.addProjectedDatabaseSize(projected.size());

            if (parameter.getBIDE().canPruneByBackScan(projected, prefix)) {
                log.countUpCanPrune();
                continue;
            }

            LoopFusedNR3PostMiningTask<T> task = new LoopFusedNR3PostMiningTask<>(projected, prefix, parameter, emitRule, log);
            threadPool.execute(task);

            bide(threadPool, projected, prefix, parameter, emitRule, log);
        }
    }
}

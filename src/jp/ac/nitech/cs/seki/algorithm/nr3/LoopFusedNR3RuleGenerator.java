package jp.ac.nitech.cs.seki.algorithm.nr3;

import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.function.Consumer;

public interface LoopFusedNR3RuleGenerator {
    <T> void generate(SequenceDatabase<T> sequenceDatabase, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> emitRule, EvaluationLogger logger);

    class RuleGenerationLog<T> {
        private long executionTime = 0L;
        private int executionTimeCount = 0;
        private int projectedDatabaseSize = 0;
        private int projectedDatabaseSizeCount = 0;
        private int allProjectedDatabaseSize = 0;
        private int allProjectedDatabaseSizeCount = 0;
        private long postMiningExecutionTime = 0L;
        private int postMiningExecutionTimeCount = 0;
        private int count_canPrune = 0;
        private int ruleCheckCount = 0;

        public void countUpCanPrune() {
            count_canPrune++;
        }

        public void addProjectedDatabaseSize(int size) {
            projectedDatabaseSize += size;
            ++projectedDatabaseSizeCount;
        }

        public void addAllProjectedDatabaseSize(int size) {
            allProjectedDatabaseSize += size;
            ++allProjectedDatabaseSizeCount;
        }

        public void addExecutionTime(long startTime, long finishTime) {
            executionTime += finishTime - startTime;
            ++executionTimeCount;
        }

        public void addPostMiningExecutionTime(long startTime, long finishTime) {
            postMiningExecutionTime += finishTime - startTime;
            ++postMiningExecutionTimeCount;
        }

        public void addRuleCheckCount(int postCondSize) {
            ruleCheckCount += postCondSize;
        }

        @Override
        public String toString() {
            double averageTime = executionTime / (double) executionTimeCount;
            double averageProj = projectedDatabaseSize / (double) projectedDatabaseSizeCount;
            double averageAllProj = allProjectedDatabaseSize / (double) allProjectedDatabaseSizeCount;
            double averagePostMiningTime = postMiningExecutionTime / (double) postMiningExecutionTimeCount;

            return "### Rule Generation Info ###\r\n" +
                    "   * Count of RuleCheck : " + ruleCheckCount + "\r\n" +
                    "   * Average Projected Database Length : " + averageProj + " (" + projectedDatabaseSizeCount + ")\r\n" +
                    "   * Average All-Projected Database Length : " + averageAllProj + " (" + allProjectedDatabaseSizeCount + ")\r\n" +
                    "   * Average PostTask Execution Time : " + averageTime + " (" + executionTimeCount + ")\r\n" +
                    "   * Average PostMining Execution Time : " + averagePostMiningTime + " (" + postMiningExecutionTimeCount + ")\r\n" +
                    "   * Count of canPruneByBS  : " + count_canPrune + "\r\n";
        }
    }
}

package jp.ac.nitech.cs.seki.algorithm.nr3.sequential;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.AbstractNR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.NR3PostMiningTask;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class SequentialNR3Miner<T> extends AbstractNR3Miner<T> {
    public SequentialNR3Miner(String name) {
        super(name);
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter) {
        return mine(seqDB, parameter, new EvaluationLogger(getName(), parameter));
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter, EvaluationLogger logger) {
        seqDB = seqDB.getOptimized(parameter.getMinSup());
        logger.addMetadata("DB Size", seqDB.size());
        logger.addMetadata("DB Avg.SeqLen", seqDB.stream().collect(Collectors.averagingLong(record -> record.getSequence().size())));
        logger.addMetadata("DB Unique Items", seqDB.getFrequentItems(parameter.getMinSup()).size());
        logger.logTimestamp("DB Optimized");

        List<Sequence<T>> preCond = new ArrayList<>();
        parameter.getBIDE().run(seqDB, parameter.getMinSup(), preCond::add);

        logger.addMetadata("PreCond Size", preCond.size());
        logger.addMetadata("PreCond Avg.Len", preCond.stream().collect(Collectors.averagingLong(List::size)));
        logger.logTimestamp("BIDE finished");

        List<Long> executionTime = new ArrayList<>();
        List<NR3Rule<T>> ruleCandidates = new LinkedList<>();
        for (Sequence<T> pre : preCond) {
            long startTime = System.currentTimeMillis();

            new NR3PostMiningTask<>(seqDB, pre, parameter, ruleCandidates::add).run();

            executionTime.add(System.currentTimeMillis() - startTime);
        }
        logger.logTimestamp("Rule generated");

        logger.addMetadata("RuleGen Avg.Time", executionTime.stream().collect(Collectors.averagingLong(i -> i)));
        logger.addMetadata("RuleCand Size", ruleCandidates.size());

        List<Rule<T>> result = Miner.removeRedundantRules(ruleCandidates);
        logger.logTimestamp("Redun. removed");

        logger.addMetadata("Rule Avg.Len", result.stream().collect(Collectors.averagingLong(r -> r.getPreCondition().size() + r.getPostCondition().size())));

        return result;
    }
}

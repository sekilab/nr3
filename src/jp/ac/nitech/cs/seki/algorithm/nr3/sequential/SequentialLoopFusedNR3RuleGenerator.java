package jp.ac.nitech.cs.seki.algorithm.nr3.sequential;

import jp.ac.nitech.cs.seki.algorithm.nr3.LoopFusedNR3PostMiningTask;
import jp.ac.nitech.cs.seki.algorithm.nr3.LoopFusedNR3RuleGenerator;
import jp.ac.nitech.cs.seki.algorithm.nr3.NR3Miner;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.List;
import java.util.function.Consumer;

public class SequentialLoopFusedNR3RuleGenerator implements LoopFusedNR3RuleGenerator {
    private static final SequentialLoopFusedNR3RuleGenerator instance = new SequentialLoopFusedNR3RuleGenerator();

    private SequentialLoopFusedNR3RuleGenerator() {
    }

    public static SequentialLoopFusedNR3RuleGenerator getInstance() {
        return instance;
    }

    private static <T> void bide(SequenceDatabase<T> optimizedDatabase, Sequence<T> sequence, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> emitRule, RuleGenerationLog<T> log) {
        List<T> frequentItems = optimizedDatabase.getFrequentItems(parameter.getMinSup());

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(item);

            SequenceDatabase<T> projected = optimizedDatabase.getProjected(item);
            log.addProjectedDatabaseSize(projected.size());

            if (parameter.getBIDE().canPruneByBackScan(projected, prefix)) {
                log.countUpCanPrune();
                continue;
            }

            new LoopFusedNR3PostMiningTask<>(projected, prefix, parameter, emitRule, log).run();

            bide(projected, prefix, parameter, emitRule, log);
        }
    }

    @Override
    public <T> void generate(SequenceDatabase<T> sequenceDatabase, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> emitRule, EvaluationLogger logger) {
        RuleGenerationLog<T> log = new RuleGenerationLog<>();
        logger.addMetadata("Rule Generation Log", log);
        bide(sequenceDatabase, Sequence.newArrayListInstance(), parameter, emitRule, log);
    }
}

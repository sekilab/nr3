package jp.ac.nitech.cs.seki.algorithm.nr3.parallel;

import jp.ac.nitech.cs.seki.algorithm.nr3.NR3PostMiningTask;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.datastructure.parallel.pool.ThreadPoolGenerator;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.concurrent.ExecutorService;
import java.util.function.Consumer;

public class PipelineNR3Miner<T> extends AbstractParallelNR3Miner<T> {

    public PipelineNR3Miner(String name, ThreadPoolGenerator threadPoolGenerator) {
        super(name, threadPoolGenerator);
    }

    @Override
    protected void getNR3Rules(ExecutorService threadPool, EvaluationLogger logger, SequenceDatabase<T> seqDB, NR3Parameter parameter, Consumer<NR3Rule<T>> printRule) {
        parameter.getBIDE().run(seqDB, parameter.getMinSup(), prefix -> {
            NR3PostMiningTask<T> task = new NR3PostMiningTask<>(seqDB, prefix, parameter, printRule);
            threadPool.execute(task);
        });

        logger.logTimestamp("BIDE finished");
    }
}

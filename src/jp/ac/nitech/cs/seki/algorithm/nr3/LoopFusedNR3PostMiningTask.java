package jp.ac.nitech.cs.seki.algorithm.nr3;

import jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialLoopFusedNR3RuleGenerator;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;

public class LoopFusedNR3PostMiningTask<T> implements Runnable {
    private final SequenceDatabase<T> projectedDatabase;
    private final Sequence<T> prefix;
    private final NR3Miner.NR3Parameter parameter;
    private final Consumer<NR3Rule<T>> printRule;
    private final SequentialLoopFusedNR3RuleGenerator.RuleGenerationLog<T> log;

    private final int prefixSequenceSupport;

    public LoopFusedNR3PostMiningTask(SequenceDatabase<T> projectedDatabase, Sequence<T> prefix, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> printRule, SequentialLoopFusedNR3RuleGenerator.RuleGenerationLog<T> log) {
        this.projectedDatabase = projectedDatabase;
        this.prefix = prefix;
        this.parameter = parameter;
        this.printRule = printRule;
        this.log = log;

        this.prefixSequenceSupport = projectedDatabase.size();
    }

    @Override
    public void run() {
        long startTime = System.currentTimeMillis();

        SequenceDatabase<T> allProjectedDatabase = projectedDatabase.getAllProjectedWithThis(prefix.get(prefix.size() - 1), true);
        log.addAllProjectedDatabaseSize(allProjectedDatabase.size());

        int allProjectedDatabaseSize = allProjectedDatabase.size();
        int bthd = (int) Math.round(parameter.getMinConf() * allProjectedDatabaseSize);
        int minSup = parameter.getMinSup();
        int minSupAll = parameter.getMinSupAll();

        List<Sequence<T>> postCond = new LinkedList<>();
        parameter.getBIDE().run(allProjectedDatabase, bthd, postCond::add);

        log.addPostMiningExecutionTime(startTime, System.currentTimeMillis());
        log.addRuleCheckCount(postCond.size());

        for (Sequence<T> post : postCond) {
            NR3Rule<T> rule = new NR3Rule<>(Sequence.newArrayListInstance(), prefix, post);

            SequenceDatabase<T> ruleAllProjected = projectedDatabase.getAllProjected(post, false);
            int iSup = ruleAllProjected.size();
            if (iSup < minSupAll) {
                continue;
            }

            int previousRecordID = -1;
            int support = 0;
            for (SequenceDatabaseRecord<T> record : ruleAllProjected) {
                if (record.getID() != previousRecordID) {
                    previousRecordID = record.getID();
                    ++support;
                }
            }

            if (support < minSup) {
                continue;
            }

            rule.setPrefixSequenceSupport(prefixSequenceSupport);
            rule.setSequenceSupport(support);
            rule.setInstanceSupport(iSup);
            rule.setConfidence((double) allProjectedDatabase.getProjected(post).size() / allProjectedDatabaseSize);
            printRule.accept(rule);
        }

        log.addExecutionTime(startTime, System.currentTimeMillis());
    }
}

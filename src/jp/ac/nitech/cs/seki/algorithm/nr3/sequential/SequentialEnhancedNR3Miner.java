package jp.ac.nitech.cs.seki.algorithm.nr3.sequential;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.*;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class SequentialEnhancedNR3Miner<T> extends AbstractNR3Miner<T> {
    public SequentialEnhancedNR3Miner(String name) {
        super(name);
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, NR3Parameter parameter, EvaluationLogger logger) {
        seqDB = seqDB.getOptimized(parameter.getMinSup());
        logger.addMetadata("DB Size", seqDB.size());
        logger.addMetadata("DB Avg.SeqLen", seqDB.stream().collect(Collectors.averagingLong(record -> record.getSequence().size())));
        logger.addMetadata("DB Unique Items", seqDB.getFrequentItems(parameter.getMinSup()).size());
//        if (seqDB instanceof CachedSequenceDatabase) {
//            logger.addMetadata("DB Avg.Table Event", seqDB.stream().collect(Collectors.averagingLong(record -> ((CachedSequenceDatabaseRecord<T>) record).getPosMap().keySet().size())));
//            logger.addMetadata("DB Avg.Table Length", seqDB.stream().collect(Collectors.averagingDouble(record -> ((CachedSequenceDatabaseRecord<T>) record).getPosMap().values().stream().collect(Collectors.averagingLong(arr -> arr.length)))));
//        }
        logger.logTimestamp("DB Optimized");

        LoopFusedNR3RuleGenerator.RuleGenerationLog<T> log = new LoopFusedNR3RuleGenerator.RuleGenerationLog<>();
        logger.addMetadata("Rule Generation Log", log);

        List<NR3Rule<T>> ruleCandidates = new LinkedList<>();
        bide(seqDB, Sequence.newArrayListInstance(), parameter, ruleCandidates::add, log, logger);

        logger.logTimestamp("Rule generated");
        logger.addMetadata("RuleCand Size", ruleCandidates.size());

        List<Rule<T>> result = Miner.removeRedundantRules(ruleCandidates);
        logger.logTimestamp("Redun. removed");

        logger.addMetadata("Rule Avg.Len", result.stream().collect(Collectors.averagingLong(r -> r.getPreCondition().size() + r.getPostCondition().size())));

        return result;
    }

    private static <T> void bide(SequenceDatabase<T> optimizedDatabase, Sequence<T> sequence, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> emitRule, LoopFusedNR3RuleGenerator.RuleGenerationLog<T> log, EvaluationLogger logger) {
        List<T> frequentItems = optimizedDatabase.getFrequentItems(parameter.getMinSup());

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(item);

            SequenceDatabase<T> projected = optimizedDatabase.getProjected(item);
            log.addProjectedDatabaseSize(projected.size());

            if (parameter.getBIDE().canPruneByBackScan(projected, prefix)) {
                log.countUpCanPrune();
                continue;
            }

            new EnhancedNR3PostMiningTask<>(projected, prefix, parameter, emitRule, log, logger).run();

            bide(projected, prefix, parameter, emitRule, log, logger);
        }
    }
}

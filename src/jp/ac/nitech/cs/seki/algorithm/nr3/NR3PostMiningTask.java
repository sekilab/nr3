package jp.ac.nitech.cs.seki.algorithm.nr3;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class NR3PostMiningTask<T> implements Runnable {
    private final SequenceDatabase<T> seqDB;
    private final Sequence<T> prefix;
    private final NR3Miner.NR3Parameter parameter;
    private final Consumer<NR3Rule<T>> printRule;

    public NR3PostMiningTask(SequenceDatabase<T> sequenceDatabase, Sequence<T> prefix, NR3Miner.NR3Parameter parameter, Consumer<NR3Rule<T>> printRule) {
        this.seqDB = sequenceDatabase;
        this.prefix = prefix;
        this.parameter = parameter;
        this.printRule = printRule;
    }

    @Override
    public void run() {
        SequenceDatabase<T> allProjected = seqDB.getAllProjected(prefix, true);
        int bthd = (int) Math.round(parameter.getMinConf() * allProjected.size());
        int minSup = parameter.getMinSup();
        int minSupAll = parameter.getMinSupAll();

        List<Sequence<T>> postCond = new ArrayList<>();
        parameter.getBIDE().run(allProjected, bthd, postCond::add);

        for (Sequence<T> post : postCond) {
            NR3Rule<T> rule = new NR3Rule<>(Sequence.newArrayListInstance(), prefix, post);

            SequenceDatabase<T> ruleAllProjected = seqDB.getAllProjected(rule, false);
            int iSup = ruleAllProjected.size();
            if (iSup < minSupAll) {
                continue;
            }

            int previousRecordID = -1;
            int support = 0;
            for (SequenceDatabaseRecord<T> record : ruleAllProjected) {
                if (record.getID() != previousRecordID) {
                    previousRecordID = record.getID();
                    ++support;
                }
            }

            if (support < minSup) {
                continue;
            }

            // rule.setPrefixSequenceSupport(seqDB.getProjected(prefix).size());
            rule.setSequenceSupport(support);
            rule.setInstanceSupport(iSup);
            rule.setConfidence((double) allProjected.getProjected(post).size() / allProjected.size());
            printRule.accept(rule);
        }
    }
}

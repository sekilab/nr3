package jp.ac.nitech.cs.seki.algorithm.ebi;

import jp.ac.nitech.cs.seki.algorithm.EvaluationParameter;
import jp.ac.nitech.cs.seki.algorithm.Miner;

public interface EBIMiner<T> extends Miner<T, EBIMiner.EBIParameter> {
    class EBIParameter implements EvaluationParameter {
        private final int minSup;
        private final int minSupAll;
        private double minConf;

        public EBIParameter(int minSup, int minSupAll, double minConf) {
            this.minSup = minSup;
            this.minSupAll = minSupAll;
            this.minConf = minConf;
        }

        @Override
        public int getMinSup() {
            return minSup;
        }

        @Override
        public int getMinSupAll() {
            return minSupAll;
        }

        @Override
        public double getMinConf() {
            return minConf;
        }

        @Override
        public String toString() {
            return "{min_sup: " + minSup + ", min_sup-all: " + minSupAll + ", min_conf: " + String.format("%.1f%%", minConf * 100) + "}";
        }
    }

    @Override
    default EBIParameter getParameter(int databaseSize, Object... params) {
        return new EBIParameter(
                (int) Math.round(((double) params[0]) * databaseSize),
                (int) params[1],
                (double) params[2]
        );
    }
}

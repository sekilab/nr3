package jp.ac.nitech.cs.seki.algorithm.ebi;

import jp.ac.nitech.cs.seki.algorithm.bide.LSSetBIDE;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;
import jp.ac.nitech.cs.seki.datastructure.impl.*;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.*;
import java.util.function.Consumer;

public class RuleGenerateTask<T> implements Runnable {
    private final SequenceDatabase<T> sequenceDatabase;
    private final EBIMiner.EBIParameter parameter;
    private final SuffixPositionTree<T> postTree;
    private final Consumer<NR3Rule<T>> printRule;
    private final EvaluationLogger logger;

    private int totalRuleCount;
    private int totalPrefixCount;
    private int totalPrefixCheckCount;
    private int totalRuleCheckCount;

    public RuleGenerateTask(SequenceDatabase<T> sequenceDatabase, EBIMiner.EBIParameter parameter, SuffixPositionTree<T> postTree, Consumer<NR3Rule<T>> printRule, EvaluationLogger logger) {
        this.sequenceDatabase = sequenceDatabase;
        this.parameter = parameter;
        this.postTree = postTree;
        this.printRule = printRule;
        this.logger = logger;
    }

    @Override
    public void run() {
        forwardGrow(sequenceDatabase, sequenceDatabase, Sequence.newArrayListInstance());
        logger.addMetadata("Total Prefix Count", totalPrefixCount);
        logger.addMetadata("Total Rule Count", totalRuleCount);
        logger.addMetadata("Total Prefix Check Count", totalPrefixCheckCount);
        logger.addMetadata("Total Rule Check Count", totalRuleCheckCount);
    }

    private void forwardGrow(SequenceDatabase<T> database, SequenceDatabase<T> parentProjected, Sequence<T> sequence) {
        List<T> frequentItems = database.getFrequentItems(parameter.getMinSup());

        for (T item : frequentItems) {
            Sequence<T> prefix = Sequence.newArrayListInstance(sequence);
            prefix.add(0, item);

            SequenceDatabase<T> preProjected = database.getPrefixProjected(item);
            SequenceDatabase<T> projected = getProjected(preProjected, prefix);
            boolean isSameProjected = hasSameProjectedDatabase(projected, parentProjected);

            boolean hasRules = false;
            boolean isBackScanPruned = LSSetBIDE.getInstance().canPruneByBackScan(preProjected, prefix);
            if (!isBackScanPruned) {
                ++totalPrefixCheckCount;

                if (hasRules = traverseSuffix(prefix, projected, postTree, Sequence.newArrayListInstance())) {
                    ++totalPrefixCount;
                }
            }

            if (isBackScanPruned || !isSameProjected || hasRules) {
                forwardGrow(preProjected, projected, prefix);
            }
        }
    }

    private boolean hasSameProjectedDatabase(SequenceDatabase<T> projected, SequenceDatabase<T> parentProjected) {
        Iterator<SequenceDatabaseRecord<T>> parentRecord = parentProjected.iterator();
        for (SequenceDatabaseRecord<T> record : projected) {
            if (!parentRecord.hasNext() || record.getID() != parentRecord.next().getID()) {
                return false;
            }
        }
        return true;
    }

    private SequenceDatabase<T> getProjected(SequenceDatabase<T> preProjected, Sequence<T> prefix) {
        SequenceDatabase<T> database = preProjected.newInstance();
        for (SequenceDatabaseRecord<T> record : preProjected) {
            Sequence<T> sequence = ((PartialSequence<T>) record.getSequence()).getOriginalSequence();
            database.addRecord(record.getID(), sequence);
        }
        return database.getProjected(prefix);
    }

    private boolean traverseSuffix(Sequence<T> prefix, SequenceDatabase<T> projected, SuffixPositionTree<T> parent, Sequence<T> post) {
        boolean hasRules = false;
        for (SuffixPositionTree<T> postNode : parent.getChildren()) {
            ++totalRuleCheckCount;

            Sequence<T> sequence = Sequence.newArrayListInstance(post);
            sequence.add(0, postNode.getValue());

            SupportConfidencePair supportConfidencePair = getSupportAndConfidence(prefix.get(prefix.size() - 1), sequence, projected, postNode.getPositionRecords());

            if (supportConfidencePair.sequenceSupport < parameter.getMinSup() ||
                supportConfidencePair.confidence < parameter.getMinConf() ||
                supportConfidencePair.instanceSupport < parameter.getMinSupAll()) {
                continue;
            }

            hasRules = true;
            NR3Rule<T> rule = new NR3Rule<>(Sequence.newArrayListInstance(), prefix, sequence, supportConfidencePair.sequenceSupport, projected.size(), supportConfidencePair.instanceSupport, supportConfidencePair.confidence);
            printRule.accept(rule);
            ++totalRuleCount;

            traverseSuffix(prefix, projected, postNode, sequence);
        }
        return hasRules;
    }

    private SupportConfidencePair getSupportAndConfidence(T prefixLastEvent, Sequence<T> post, SequenceDatabase<T> projected, List<PositionRecord> positionRecords) {
        int sup = 0;
        int postPreAll = 0;
        int preAll = 0;
        int supAll = 0;

        List<Integer> support = new ArrayList<>();

        Iterator<SequenceDatabaseRecord<T>> recordIterator = projected.iterator();
        Iterator<PositionRecord> postIterator = positionRecords.iterator();
        SequenceDatabaseRecord<T> record = null;
        PositionRecord postRecord = null;
        while((record != null || recordIterator.hasNext()) && (postRecord != null || postIterator.hasNext())) {
            if (record == null) {
                record = recordIterator.next();
            }
            if (postRecord == null) {
                postRecord = postIterator.next();
            }
            if (record.getID() > postRecord.getID()) {
                postRecord = null;
                continue;
            }

            PartialSequence<T> partialSequence = (PartialSequence<T>) record.getSequence();
            Sequence<T> sequence = partialSequence.getOriginalSequence();
            int preLastIndex = partialSequence.getBeginIndex() - 1;
            int sequenceSize = sequence.size();

            if (record.getID() < postRecord.getID()) {
                // confidence
                for (int i = preLastIndex; i < sequenceSize; ++i) {
                    if (prefixLastEvent.equals(sequence.get(i))) {
                        support.add(record.getID());
                        ++preAll;
                    }
                }
                record = null;
                continue;
            }

            int postFirstIndex = postRecord.getPosition(); // index of first(post)

            int recordID = record.getID();

            record = null;
            postRecord = null;

            // confidence
            for (int i = preLastIndex; i < sequenceSize; ++i) {
                if (prefixLastEvent.equals(sequence.get(i))) {
                    ++preAll;
                    if (i < postFirstIndex) {
                        ++postPreAll;
                    }
                    support.add(recordID);
                }
            }

            // sequence support
            if (postFirstIndex <= preLastIndex) {
                continue;
            }
            ++sup;

            // instance support;
            int cur = preLastIndex + 1;
            for (T event : post) {
                for (; cur < sequenceSize; ++cur) {
                    if (event.equals(sequence.get(cur))) {
                        break;
                    }
                }
                if (cur == sequenceSize) {
                    break;
                }
            }
            if (cur != sequenceSize) {
                T lastPostEvent = post.get(post.size() - 1);
                for (; cur < sequenceSize; ++cur) {
                    if (lastPostEvent.equals(sequence.get(cur))) {
                        ++supAll;
                    }
                }
            }
        }
        while (record != null || recordIterator.hasNext()) {
            if (record == null) {
                record = recordIterator.next();
            }
            PartialSequence<T> partialSequence = (PartialSequence<T>) record.getSequence();
            Sequence<T> sequence = partialSequence.getOriginalSequence();
            int preLastIndex = partialSequence.getBeginIndex() - 1;
            int sequenceSize = sequence.size();
            for (int i = preLastIndex; i < sequenceSize; ++i) {
                if (prefixLastEvent.equals(sequence.get(i))) {
                    support.add(record.getID());
                    ++preAll;
                }
            }
            record = null;
        }
        return new SupportConfidencePair(sup, supAll, (double) postPreAll / preAll);
    }

    private class SupportConfidencePair {
        int sequenceSupport;
        int instanceSupport;
        double confidence;
        SupportConfidencePair(int sequenceSupport, int instanceSupport, double confidence) {
            this.sequenceSupport = sequenceSupport;
            this.instanceSupport = instanceSupport;
            this.confidence = confidence;
        }
    }
}

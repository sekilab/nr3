package jp.ac.nitech.cs.seki.algorithm.ebi;

public abstract class AbstractEBIMiner<T> implements EBIMiner<T> {
    private final String name;

    protected AbstractEBIMiner(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}

package jp.ac.nitech.cs.seki.algorithm.ebi;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.datastructure.impl.SuffixPositionTree;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class SequentialEBIMiner<T> extends AbstractEBIMiner<T> {
    public SequentialEBIMiner(String name) {
        super(name);
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, EBIParameter parameter) {
        return mine(seqDB, parameter, new EvaluationLogger(getName(), parameter));
    }

    @Override
    public List<Rule<T>> mine(SequenceDatabase<T> seqDB, EBIParameter parameter, EvaluationLogger logger) {
        SequenceDatabase<T> sequenceDatabase = seqDB.getOptimized(parameter.getMinSup());
        logger.logTimestamp("SeqDB Optimized");

        // BackwardGrow
        SuffixPositionTree<T> postTree = new SuffixPositionTree<>(null, null, null);
        new BackwardGrowTask<>(sequenceDatabase, parameter, postTree, logger).run();
        logger.logTimestamp("BackwardGrow Done");

        // RuleGenerate
        Collection<NR3Rule<T>> ruleCandidates = new LinkedList<>();
        new RuleGenerateTask<>(sequenceDatabase, parameter, postTree, ruleCandidates::add, logger).run();
        logger.logTimestamp("RuleGenerate Done");

        List<Rule<T>> result = Miner.removeRedundantRules(ruleCandidates);
        logger.logTimestamp("Redun. Removed");

        return result;
    }
}

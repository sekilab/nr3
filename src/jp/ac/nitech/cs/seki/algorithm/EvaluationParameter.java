package jp.ac.nitech.cs.seki.algorithm;

public interface EvaluationParameter {
    int getMinSup();

    int getMinSupAll();

    double getMinConf();
}

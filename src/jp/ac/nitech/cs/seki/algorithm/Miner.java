package jp.ac.nitech.cs.seki.algorithm;

import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.impl.NR3Rule;
import jp.ac.nitech.cs.seki.evaluator.EvaluationLogger;

import java.util.*;

import static jp.ac.nitech.cs.seki.algorithm.Constants.EPS;

public interface Miner<T, P extends EvaluationParameter> {
    String getName();

    /**
     * Mine Non Redundant Recurrent Rules
     *
     * @param seqDB     Sequence DB
     * @param parameter Miner parameter
     * @return None Redundant Set of Recurrent Rules
     */
    List<Rule<T>> mine(SequenceDatabase<T> seqDB, P parameter);

    /**
     * Mine and log Non Redundant Recurrent Rules
     *
     * @param seqDB     Sequence DB
     * @param parameter Miner parameter
     * @param logger    Evaluation Logger
     * @return None Redundant Set of Recurrent Rules
     */
    List<Rule<T>> mine(SequenceDatabase<T> seqDB, P parameter, EvaluationLogger logger);

    P getParameter(int databaseSize, Object... params);

    static <T> List<Rule<T>> removeRedundantRules(Collection<NR3Rule<T>> ruleList) {
        Map<Integer, List<NR3Rule<T>>> hashTable = new HashMap<>();
        for (NR3Rule<T> rx : ruleList) {
            int hashCode = rx.hashCode();
            List<NR3Rule<T>> list = hashTable.computeIfAbsent(hashCode, k -> new LinkedList<>());
            Iterator<NR3Rule<T>> listIterator = list.iterator();
            boolean isRxRedundant = false;

            while (listIterator.hasNext()) {
                NR3Rule<T> ry = listIterator.next();

                if (rx.getSequenceSupport() != ry.getSequenceSupport() || rx.getInstanceSupport() != ry.getInstanceSupport() || Math.abs(rx.getConfidence() - ry.getConfidence()) >= EPS) {
                    continue;
                }

                if (rx.equals(ry)) {
                    if (rx.getPreCondition().size() >= ry.getPreCondition().size()) {
                        isRxRedundant = true;
                    } else {
                        listIterator.remove();
                    }
                } else if (rx.isSubsequenceOf(ry)) {
                    isRxRedundant = true;
                } else if (ry.isSubsequenceOf(rx)) {
                    listIterator.remove();
                }
            }
            if (!isRxRedundant) {
                list.add(rx);
            }
        }

        List<Rule<T>> rules = new ArrayList<>();
        for (List<NR3Rule<T>> list : hashTable.values()) {
            rules.addAll(list);
        }
        return rules;
    }
}

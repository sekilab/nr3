package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.FrequentItemSet;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class EBISequenceDatabase<T> implements SequenceDatabase<T> {
    private final Collection<EBISequenceDatabaseRecord<T>> container;
    private final Supplier<Collection<EBISequenceDatabaseRecord<T>>> generateContainer;
    private final Supplier<Sequence<T>> generateSequence;

    public EBISequenceDatabase(Supplier<Collection<EBISequenceDatabaseRecord<T>>> generateContainer, Supplier<Sequence<T>> generateSequence) {
        this.container = generateContainer.get();
        this.generateContainer = generateContainer;
        this.generateSequence = generateSequence;
    }

    public static EBISequenceDatabase<Integer> newIntegerLinkedListInstance(String fileName, Supplier<Sequence<Integer>> generateSequence) {
        return loadFromFile(fileName, () -> new LinkedList<>(), generateSequence, Integer::parseInt);
    }

    public static EBISequenceDatabase<String> newStringLinkedListInstance(String fileName, Supplier<Sequence<String>> generateSequence) {
        return loadFromFile(fileName, () -> new LinkedList<>(), generateSequence, s -> s);
    }

    public static <T> EBISequenceDatabase<T> loadFromFile(String fileName, Supplier<Collection<EBISequenceDatabaseRecord<T>>> generateContainer, Supplier<Sequence<T>> generateSequence, Function<String, T> parseItem) {
        EBISequenceDatabase<T> database = new EBISequenceDatabase<>(generateContainer, generateSequence);
        try (
                FileInputStream fin = new FileInputStream(new File(fileName));
                BufferedReader myInput = new BufferedReader(new InputStreamReader(fin))
        ) {
            String thisLine;
            int id = 0;
            while ((thisLine = myInput.readLine()) != null) {
                if (!thisLine.isEmpty() && thisLine.charAt(0) != '#' && thisLine.charAt(0) != '%' && thisLine.charAt(0) != '@') {
                    database.container.add(parseRecord(database.generateSequence.get(), ++id, thisLine.split(" "), parseItem));
                }
            }
        } catch (IOException | ParseException exception) {
            throw new RuntimeException(exception);
        }
        return database;
    }

    private static <T> EBISequenceDatabaseRecord<T> parseRecord(Sequence<T> sequence, int id, String[] tokens, Function<String, T> parseItem) throws ParseException {
        int pos = 0;
        Map<T, List<Integer>> posMap = new HashMap<>();
        for (String token : tokens) {
            // assume each record consists of itemsets with a single item.
            // if the token is -1, it means that we reached the end of an itemset.
            switch (token) {
                case "-1":
                    // ignore
                    break;
                // if the token is -2, it means that we reached the end of the sequence.
                case "-2":
                    Map<T, int[]> newPosMap = new HashMap<>();
                    posMap.forEach((key, value) -> {
                        newPosMap.put(key, value.stream().mapToInt(i -> i).toArray());
                    });
                    return new EBISequenceDatabaseRecord<>(id, sequence, newPosMap);
                default:
                    // otherwise it is an item.
                    T item = parseItem.apply(token);
                    sequence.add(item);

                    posMap.computeIfAbsent(item, k -> new ArrayList<>()).add(pos++);
                    break;
            }
        }
        throw new ParseException("no -2", 0);
    }

    @Override
    public SequenceDatabase<T> getProjected(Sequence<T> pattern) {
        throw new UnsupportedOperationException();
    }

    @Override
    public SequenceDatabase<T> getProjected(T event) {
        throw new UnsupportedOperationException();
    }

    @Override
    public SequenceDatabase<T> getAllProjected(Sequence<T> pattern, boolean useNewNumber) {
        throw new UnsupportedOperationException();
    }

    @Override
    public SequenceDatabase<T> getAllProjectedWithThis(T event, boolean useNewNumber) {
        throw new UnsupportedOperationException();
    }

    @Override
    public EBISequenceDatabase<T> getPrefixProjected(T event) {
        EBISequenceDatabase<T> database = newInstance();
        for (EBISequenceDatabaseRecord<T> record : container) {
            int lastIndex = record.getLastIndex();
            if (lastIndex < 0) {
                continue;
            }

            Sequence<T> sequence = record.getSequence();
            Map<T, int[]> posMap = record.getPosMap();
            int[] posSet = posMap.get(event);
            if (posSet == null || posSet[0] > lastIndex) {
                continue;
            }

            int y = 0;
            for (; y < posSet.length; ++y) {
                if (posSet[y] > lastIndex) {
                    int pos = posSet[y - 1] - 1;
                    database.addRecord(record.getID(), sequence, posMap, pos);
                    break;
                }
            }
            if (y == posSet.length) {
                int pos = posSet[y - 1] - 1;
                database.addRecord(record.getID(), sequence, posMap, pos);
            }
        }
        return database;
    }

    @Override
    public SequenceDatabase<T> getPrefixProjected(Sequence<T> pattern) {
        throw new UnsupportedOperationException();
    }

    @Override
    public EBISequenceDatabase<T> newInstance() {
        return new EBISequenceDatabase<>(generateContainer, generateSequence);
    }

    public Collection<EBISequenceDatabaseRecord<T>> getContainer() {
        return container;
    }

    @Override
    public EBISequenceDatabase<T> getOptimized(int minSup) {
        Set<T> frequentItems = new HashSet<>(getFrequentItems(minSup));
        EBISequenceDatabase<T> database = newInstance();

        for (EBISequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = generateSequence.get();
            int pos = 0;
            Map<T, List<Integer>> posMap = new HashMap<>();
            for (T item : record.getSequence()) {
                if (frequentItems.contains(item)) {
                    sequence.add(item);
                    posMap.computeIfAbsent(item, k -> new ArrayList<>()).add(pos++);
                }
            }
            if (sequence.size() > 0) {
                Map<T, int[]> newPosMap = new HashMap<>();
                posMap.forEach((key, value) -> {
                    newPosMap.put(key, value.stream().mapToInt(i -> i).toArray());
                });
                database.addRecord(record.getID(), sequence, newPosMap, sequence.size() - 1);
            }
        }
        return database;
    }

    @Override
    public List<T> getFrequentItems(int minSup) {
        FrequentItemSet<T> frequentItemSet = new FrequentItemSet<>();

        for (EBISequenceDatabaseRecord<T> record : container) {
            frequentItemSet.addAll(record.getSequence().getPrefix(record.getLastIndex() + 1, true).getFrequentItems());
        }

        List<T> frequentItems = new ArrayList<>();
        frequentItemSet.getItemMap().forEach((item, count) -> {
            if (count >= minSup) {
                frequentItems.add(item);
            }
        });
        return frequentItems;
    }

    /**
     * params[0] : Map<T, int[]> posMap
     * params[1] : int length
     */
    @SuppressWarnings("unchecked")
    @Override
    public void addRecord(int id, Sequence<T> sequence, Object... params) {
        if (params.length == 2 /* && params[0] instanceof Map && params[1] instanceof Integer */) {
            container.add(new EBISequenceDatabaseRecord<>(id, sequence, (Map<T, int[]>) params[0], (int) params[1]));
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public boolean isEmpty() {
        return container.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return container.contains(o);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterator<SequenceDatabaseRecord<T>> iterator() {
        return (Iterator<SequenceDatabaseRecord<T>>) (Iterator<? extends SequenceDatabaseRecord<T>>) container.iterator();
    }

    @Override
    public Object[] toArray() {
        return container.toArray();
    }

    @Override
    public <V> V[] toArray(V[] a) {
        return container.toArray(a);
    }

    @Override
    public boolean add(SequenceDatabaseRecord<T> sequenceDatabaseRecord) {
        return container.add((EBISequenceDatabaseRecord<T>) sequenceDatabaseRecord);
    }

    @Override
    public boolean remove(Object o) {
        return container.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return container.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends SequenceDatabaseRecord<T>> c) {
        return container.addAll((Collection<? extends EBISequenceDatabaseRecord<T>>) c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return container.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return container.retainAll(c);
    }

    @Override
    public void clear() {
        container.clear();
    }
}

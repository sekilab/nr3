package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.DerivedSequence;
import jp.ac.nitech.cs.seki.datastructure.Sequence;

public class PartialSequence<T> extends SequenceImpl<T> implements DerivedSequence<T> {
    private final Sequence<T> sequence;
    private final int beginIndex;
    private final int endIndex;

    /**
     * Generate PartialSequence (immutable)
     *
     * @param sequence        parent sequence
     * @param beginIndex      begin index (inclusive) of the parent sequence
     * @param endIndex        end index (inclusive) of the parent sequence
     * @param overwriteParent if true, set the parent sequence as generated PartialSequence
     */
    public PartialSequence(Sequence<T> sequence, int beginIndex, int endIndex, boolean overwriteParent) {
        super(sequence.subList(beginIndex, endIndex + 1));

        if (overwriteParent) {
            this.sequence = this;
            this.beginIndex = 0;
            this.endIndex = this.size() - 1;
        } else {
            if (sequence instanceof PartialSequence) {
                PartialSequence<T> parent = (PartialSequence<T>) sequence;
                this.sequence = parent.sequence;
                this.beginIndex = parent.beginIndex + beginIndex;
                this.endIndex = parent.beginIndex + endIndex;
            } else {
                this.sequence = sequence;
                this.beginIndex = beginIndex;
                this.endIndex = endIndex;
            }
        }
    }

    public PartialSequence(Sequence<T> sequence, int beginIndex, int endIndex) {
        this(sequence, beginIndex, endIndex, false);
    }

    @Override
    public Sequence<T> getOriginalSequence() {
        return sequence;
    }

    public int getBeginIndex() {
        return beginIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("< ");
        forEach(item -> builder.append(item).append(" "));
        builder.append(">");
        return builder.toString();
    }
}

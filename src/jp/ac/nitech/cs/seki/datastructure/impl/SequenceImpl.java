package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Sequence;

import java.util.*;

public class SequenceImpl<T> implements Sequence<T>, RandomAccess {
    private final List<T> container;

    public SequenceImpl(List<T> container) {
        this.container = container;
    }

    public SequenceImpl(List<T> container, Sequence<T> sequence) {
        this.container = container;
        this.container.addAll(sequence);
    }

    @Override
    public Set<T> getFrequentItems() {
        return new HashSet<>(container);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("< ");
        forEach(item -> builder.append(item).append(" "));
        builder.append(">");
        return builder.toString();
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public boolean isEmpty() {
        return container.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return container.contains(o);
    }

    @Override
    public Iterator<T> iterator() {
        return container.iterator();
    }

    @Override
    public Object[] toArray() {
        return container.toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return container.toArray(a);
    }

    @Override
    public boolean add(T t) {
        return container.add(t);
    }

    @Override
    public boolean remove(Object o) {
        return container.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return container.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return container.addAll(c);
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return container.addAll(index, c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return container.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return container.retainAll(c);
    }

    @Override
    public void clear() {
        container.clear();
    }

    @Override
    public T get(int index) {
        return container.get(index);
    }

    @Override
    public T set(int index, T element) {
        return container.set(index, element);
    }

    @Override
    public void add(int index, T element) {
        container.add(index, element);
    }

    @Override
    public T remove(int index) {
        return container.remove(index);
    }

    @Override
    public int indexOf(Object o) {
        return container.indexOf(o);
    }

    @Override
    public int lastIndexOf(Object o) {
        return container.lastIndexOf(o);
    }

    @Override
    public ListIterator<T> listIterator() {
        return container.listIterator();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return container.listIterator(index);
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        return container.subList(fromIndex, toIndex);
    }

    @Override
    public boolean equals(Object obj) {
        return container.equals(obj);
    }
}

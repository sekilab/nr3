package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Sequence;

import java.util.*;

public class CachedSequenceDatabaseRecord<T> extends DefaultSequenceDatabaseRecord<T> {
    private final Map<T, int[]> posMap;
    private final int offset;  // sequence のoriginal seq.における開始位置
    private final int parentOffset;

    public CachedSequenceDatabaseRecord(int id, Sequence<T> sequence) {
        super(id, sequence);
        this.posMap = new HashMap<>();
        this.offset = 0;
        this.parentOffset = 0;
        Map<T, List<Integer>> posMap = new HashMap<>();
        int sequenceSize = sequence.size();
        for (int i = 0; i < sequenceSize; ++i) {
            T event = sequence.get(i);
            posMap.computeIfAbsent(event, k -> new ArrayList<>()).add(i);
        }
        posMap.forEach((key, value) -> {
            Collections.reverse(value);
            this.posMap.put(key, value.stream().mapToInt(i -> i).toArray());
        });
    }

    public CachedSequenceDatabaseRecord(int id, Sequence<T> sequence, Map<T, int[]> posMap) {
        super(id, sequence);
        this.posMap = posMap;
        this.offset = 0;
        this.parentOffset = 0;
    }

    public CachedSequenceDatabaseRecord(int id, Sequence<T> sequence, Map<T, int[]> posMap, int offset) {
        super(id, sequence);
        this.posMap = posMap;
        this.offset = offset;
        this.parentOffset = offset;
    }

    public CachedSequenceDatabaseRecord(int id, Sequence<T> sequence, Map<T, int[]> posMap, int offset, int parentOffset) {
        super(id, sequence);
        this.posMap = posMap;
        this.offset = offset;
        this.parentOffset = parentOffset;
    }

    public Map<T, int[]> getPosMap() {
        return posMap;
    }

    public int getOffSet() {
        return offset;
    }

    public int getParentOffset() {
        return parentOffset;
    }
}

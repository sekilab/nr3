package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Sequence;

import java.util.Map;

public class EBISequenceDatabaseRecord<T> extends DefaultSequenceDatabaseRecord<T> {
    private final Map<T, int[]> posMap;
    private final int lastIndex;

    public EBISequenceDatabaseRecord(int id, Sequence<T> sequence, Map<T, int[]> posMap) {
        super(id, sequence);
        this.posMap = posMap;
        this.lastIndex = sequence.size() - 1;
    }

    public EBISequenceDatabaseRecord(int id, Sequence<T> sequence, Map<T, int[]> posMap, int lastIndex) {
        super(id, sequence);
        this.posMap = posMap;
        this.lastIndex = lastIndex;
    }

    public Map<T, int[]> getPosMap() {
        return posMap;
    }

    public int getLastIndex() {
        return lastIndex;
    }
}

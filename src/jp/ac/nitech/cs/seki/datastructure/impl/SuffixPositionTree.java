package jp.ac.nitech.cs.seki.datastructure.impl;

import java.util.ArrayList;
import java.util.List;

public class SuffixPositionTree<T> {
    private final SuffixPositionTree<T> parent;
    private final List<SuffixPositionTree<T>> children;
    private final T value;
    private final List<PositionRecord> positionRecords;

    public SuffixPositionTree(SuffixPositionTree<T> parent, T value, List<PositionRecord> positionRecords) {
        this.parent = parent;
        this.value = value;
        this.positionRecords = positionRecords;
        this.children = new ArrayList<>();
    }

    public SuffixPositionTree<T> getParent() {
        return parent;
    }

    public List<SuffixPositionTree<T>> getChildren() {
        return children;
    }

    public T getValue() {
        return value;
    }

    public List<PositionRecord> getPositionRecords() {
        return positionRecords;
    }

    public SuffixPositionTree<T> addChild(T value, List<PositionRecord> positionRecords) {
        SuffixPositionTree<T> child = new SuffixPositionTree<>(this, value, positionRecords);
        children.add(child);
        return child;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(value);
        if (positionRecords != null) {
            builder.append("(").append(positionRecords.size()).append("):");
        }
        builder.append("[");
        children.forEach(node -> builder.append(node).append(" "));
        builder.append("]");
        return builder.toString();
    }
}

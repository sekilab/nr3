package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;

public class DefaultSequenceDatabaseRecord<T> implements SequenceDatabaseRecord<T> {
    private final int id;
    private final Sequence<T> sequence;

    public DefaultSequenceDatabaseRecord(int id, Sequence<T> sequence) {
        this.id = id;
        this.sequence = sequence;
    }

    @Override
    public int getID() {
        return id;
    }

    @Override
    public Sequence<T> getSequence() {
        return sequence;
    }

    @Override
    public String toString() {
        return String.format("(%d, %s)", id, sequence);
    }
}

package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.DerivedSequence;
import jp.ac.nitech.cs.seki.datastructure.Sequence;

public class ReversedSequence<T> extends SequenceImpl<T> implements DerivedSequence<T> {
    private final Sequence<T> sequence;

    public ReversedSequence(Sequence<T> sequence) {
        super(sequence);
        this.sequence = sequence;
    }

    @Override
    public Sequence<T> getOriginalSequence() {
        return sequence;
    }

    @Override
    public T get(int index) {
        return super.get(size() - index - 1);
    }

    @Override
    public T set(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void add(int index, T element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public T remove(int index) {
        throw new UnsupportedOperationException();
    }
}

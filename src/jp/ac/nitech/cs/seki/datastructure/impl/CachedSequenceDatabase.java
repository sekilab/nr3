package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.FrequentItemSet;
import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class CachedSequenceDatabase<T> extends AbstractCollection<SequenceDatabaseRecord<T>> implements SequenceDatabase<T> {
    private final Collection<CachedSequenceDatabaseRecord<T>> container;
    private final Supplier<Collection<CachedSequenceDatabaseRecord<T>>> generateContainer;
    private final Supplier<Sequence<T>> generateSequence;

    public CachedSequenceDatabase(Supplier<Collection<CachedSequenceDatabaseRecord<T>>> generateContainer, Supplier<Sequence<T>> generateSequence) {
        this.container = generateContainer.get();
        this.generateContainer = generateContainer;
        this.generateSequence = generateSequence;
    }

    public static CachedSequenceDatabase<Integer> newIntegerLinkedListInstance(String fileName, Supplier<Sequence<Integer>> generateSequence) {
        return loadFromFile(fileName, () -> new LinkedList<>(), generateSequence, Integer::parseInt);
    }

    public static CachedSequenceDatabase<String> newStringLinkedListInstance(String fileName, Supplier<Sequence<String>> generateSequence) {
        return loadFromFile(fileName, () -> new LinkedList<>(), generateSequence, s -> s);
    }

    public static <T> CachedSequenceDatabase<T> loadFromFile(String fileName, Supplier<Collection<CachedSequenceDatabaseRecord<T>>> generateContainer, Supplier<Sequence<T>> generateSequence, Function<String, T> parseItem) {
        CachedSequenceDatabase<T> database = new CachedSequenceDatabase<>(generateContainer, generateSequence);
        try (
                FileInputStream fin = new FileInputStream(new File(fileName));
                BufferedReader myInput = new BufferedReader(new InputStreamReader(fin))
        ) {
            String thisLine;
            int id = 0;
            while ((thisLine = myInput.readLine()) != null) {
                if (!thisLine.isEmpty() && thisLine.charAt(0) != '#' && thisLine.charAt(0) != '%' && thisLine.charAt(0) != '@') {
                    database.add(parseRecord(database.generateSequence.get(), ++id, thisLine.split(" "), parseItem));
                }
            }
        } catch (IOException | ParseException exception) {
            throw new RuntimeException(exception);
        }
        return database;
    }

    private static <T> CachedSequenceDatabaseRecord<T> parseRecord(Sequence<T> sequence, int id, String[] tokens, Function<String, T> parseItem) throws ParseException {
        int pos = 0;
        Map<T, List<Integer>> posMap = new HashMap<>();
        for (String token : tokens) {
            // assume each record consists of itemsets with a single item.
            // if the token is -1, it means that we reached the end of an itemset.
            switch (token) {
                case "-1":
                    // ignore
                    break;
                // if the token is -2, it means that we reached the end of the sequence.
                case "-2":
                    Map<T, int[]> newPosMap = new HashMap<>();
                    posMap.forEach((key, value) -> {
                        Collections.reverse(value);
                        newPosMap.put(key, value.stream().mapToInt(i -> i).toArray());
                    });
                    return new CachedSequenceDatabaseRecord<>(id, sequence, newPosMap);
                default:
                    // otherwise it is an item.
                    T item = parseItem.apply(token);
                    sequence.add(item);

                    posMap.computeIfAbsent(item, k -> new ArrayList<>()).add(pos++);
                    break;
            }
        }
        throw new ParseException("no -2", 0);
    }

    @Override
    public SequenceDatabase<T> getProjected(Sequence<T> pattern) {
        CachedSequenceDatabase<T> seqDB = newInstance();
        if (pattern.isEmpty()) {
            seqDB.addAll(this);
            return seqDB;
        }
        int patternSize = pattern.size();
        for (CachedSequenceDatabaseRecord<T> record : this.container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0 || sequenceSize < patternSize) {
                continue;
            }

            Map<T, int[]> posMap = record.getPosMap();
            Map<T, Integer> prevPosMap = new HashMap<>();
            int offset = record.getOffSet();

            int y_pre = offset - 1;
            boolean skipThisRecord = false;
            for (T value : pattern) {
                int[] posSet = posMap.get(value);
                if (posSet == null) {
                    skipThisRecord = true;
                    break;
                }

                int prevPos = prevPosMap.getOrDefault(value, posSet.length);
                if (prevPos == 0 || posSet[0] <= y_pre) {
                    skipThisRecord = true;
                    break;
                }
                int y = 0;
                for (; y < prevPos; y++) {
                    if (posSet[y] <= y_pre) {
                        y_pre = posSet[y - 1];
                        prevPosMap.put(value, y - 1);
                        break;
                    }
                }
                if (y == prevPos) {
                    y_pre = posSet[y - 1];
                    prevPosMap.put(value, y - 1);
                }
            }
            if (!skipThisRecord) {
                seqDB.addRecord(record.getID(), sequence.getSuffix(sequenceSize - (y_pre - offset) - 1, false), posMap, y_pre + 1, record.getParentOffset());
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getProjected(T event) {
        CachedSequenceDatabase<T> seqDB = newInstance();
        for (CachedSequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0) {
                continue;
            }

            int offset = record.getOffSet();
            Map<T, int[]> posMap = record.getPosMap();
            int[] posSet = posMap.get(event);
            if (posSet == null || posSet[0] < offset) {
                continue;
            }

            int y = 0;
            for (; y < posSet.length; y++) {
                if (posSet[y] < offset) {
                    int pos = posSet[y - 1];
                    seqDB.addRecord(record.getID(), sequence.getSuffix(sequenceSize - (pos - offset) - 1, false), posMap, pos + 1, record.getParentOffset());
                    break;
                }
            }
            if (y == posSet.length) {
                int pos = posSet[y - 1];
                seqDB.addRecord(record.getID(), sequence.getSuffix(sequenceSize - (pos - offset) - 1, false), posMap, pos + 1, record.getParentOffset());
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getAllProjected(Sequence<T> pattern, boolean useNewNumber) {
        CachedSequenceDatabase<T> seqDB = newInstance();
        int recordNumber = 0;
        if (pattern.isEmpty()) {
            for (CachedSequenceDatabaseRecord<T> record : container) {
                seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), record.getSequence().getSuffix(record.getSequence().size(), true), record.getPosMap(), record.getOffSet());
            }
            return seqDB;
        }
        int patternSize = pattern.size();
        for (CachedSequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0 || sequenceSize < patternSize) {
                continue;
            }

            int offset = record.getOffSet();
            Map<T, int[]> posMap = record.getPosMap();
            Map<T, Integer> prevPosMap = new HashMap<>();
            int y = 0;
            int y_pre = offset - 1;
            boolean skipThisRecord = false;
            int[] posSet = null;
            for (T value : pattern) {
                posSet = posMap.get(value);
                if (posSet == null) {
                    skipThisRecord = true;
                    break;
                }

                int prevPos = prevPosMap.getOrDefault(value, posSet.length);
                if (prevPos == 0 || posSet[0] <= y_pre) {
                    skipThisRecord = true;
                    break;
                }
                y = 0;
                for (; y < prevPos; ++y) {
                    if (posSet[y] <= y_pre) {
                        y_pre = posSet[y - 1];
                        prevPosMap.put(value, y - 1);
                        break;
                    }
                }
                if (y == prevPos) {
                    y_pre = posSet[y - 1];
                    prevPosMap.put(value, y - 1);
                }
            }
            if (!skipThisRecord) {
                y_pre = y;
                for (y = 0; y < y_pre; ++y) {
                    int pos = posSet[y];
                    seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), sequence.getSuffix(sequenceSize - (pos - offset) - 1, true), posMap, pos + 1);
                }
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getAllProjectedWithThis(T event, boolean useNewNumber) {
        CachedSequenceDatabase<T> seqDB = newInstance();
        int recordNumber = 0;
        for (CachedSequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            Map<T, int[]> posMap = record.getPosMap();
            int offset = record.getOffSet();
            int sequenceSize = sequence.size();

            seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), sequence.getSuffix(sequenceSize, true), posMap, offset);
            if (sequenceSize <= 0) {
                continue;
            }

            int[] posSet = posMap.get(event);
            if (posSet != null) {
                for (int pos : posSet) {
                    if (pos < offset) {
                        break;
                    }
                    seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), sequence.getSuffix(sequenceSize - (pos - offset) - 1, true), posMap, pos + 1);
                }
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getPrefixProjected(T event) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public SequenceDatabase<T> getPrefixProjected(Sequence<T> pattern) {
        throw new RuntimeException("Not implemented");
    }

    /**
     * params[0] : Map<T, int[]> posMap
     * params[1] : int offset (ignorable)
     * params[2] : int parentOffset (ignorable)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void addRecord(int id, Sequence<T> sequence, Object... params) {
        if (params.length == 3 /* && params[0] instanceof int[] && params[1] instanceof int && params[2] instanceof int */) {
            container.add(new CachedSequenceDatabaseRecord<>(id, sequence, (Map<T, int[]>) params[0], (int) params[1], (int) params[2]));
        } else if (params.length == 2 /* && params[0] instanceof int[] && params[1] instanceof int */) {
            container.add(new CachedSequenceDatabaseRecord<>(id, sequence, (Map<T, int[]>) params[0], (int) params[1]));
        } else if (params.length == 1 /* && params[0] instanceof int[] */) {
            container.add(new CachedSequenceDatabaseRecord<>(id, sequence, (Map<T, int[]>) params[0]));
        } else if (params.length == 0) {
            container.add(new CachedSequenceDatabaseRecord<>(id, sequence));
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public CachedSequenceDatabase<T> getOptimized(int minSup) {
        List<T> frequentItemList = getFrequentItems(minSup);
        Set<T> frequentItems = new HashSet<>(frequentItemList);
        CachedSequenceDatabase<T> optimizedSequenceDatabase = newInstance();

        for (CachedSequenceDatabaseRecord<T> record : container) {
            Sequence<T> optimizedSequence = generateSequence.get();
            int pos = 0;
            Map<T, List<Integer>> posMap = new HashMap<>();
            for (T item : record.getSequence()) {
                if (frequentItems.contains(item)) {
                    optimizedSequence.add(item);
                    posMap.computeIfAbsent(item, k -> new ArrayList<>()).add(pos++);
                }
            }
            if (optimizedSequence.size() > 0) {
                Map<T, int[]> newPosMap = new HashMap<>();
                posMap.forEach((key, value) -> {
                    Collections.reverse(value);
                    newPosMap.put(key, value.stream().mapToInt(i -> i).toArray());
                });
                optimizedSequenceDatabase.addRecord(record.getID(), optimizedSequence, newPosMap);
            }
        }
        return optimizedSequenceDatabase;
    }

    @Override
    public List<T> getFrequentItems(int minSup) {
        FrequentItemSet<T> frequentItemSet = new FrequentItemSet<>();

        for (SequenceDatabaseRecord<T> record : container) {
            frequentItemSet.addAll(record.getSequence().getFrequentItems());
        }

        List<T> frequentItems = new ArrayList<>();
        frequentItemSet.getItemMap().forEach((item, count) -> {
            if (count >= minSup) {
                frequentItems.add(item);
            }
        });
        return frequentItems;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        container.forEach(record -> builder.append(record.toString()).append("\n"));
        return builder.toString();
    }

    public Collection<CachedSequenceDatabaseRecord<T>> getContainer() {
        return this.container;
    }

    @Override
    public CachedSequenceDatabase<T> newInstance() {
        return new CachedSequenceDatabase<>(generateContainer, generateSequence);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterator<SequenceDatabaseRecord<T>> iterator() {
        return (Iterator<SequenceDatabaseRecord<T>>) (Iterator<? extends SequenceDatabaseRecord<T>>) container.iterator();
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public boolean add(SequenceDatabaseRecord<T> e) {
        return container.add((CachedSequenceDatabaseRecord<T>) e);
    }
}

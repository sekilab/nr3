package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Sequence;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabaseRecord;

import java.io.*;
import java.text.ParseException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class DefaultSequenceDatabase<T> extends AbstractCollection<SequenceDatabaseRecord<T>> implements SequenceDatabase<T> {
    private final Collection<SequenceDatabaseRecord<T>> container;
    private final Supplier<Collection<SequenceDatabaseRecord<T>>> generateContainer;
    private final Supplier<Sequence<T>> generateSequence;

    public DefaultSequenceDatabase(Supplier<Collection<SequenceDatabaseRecord<T>>> generateContainer, Supplier<Sequence<T>> generateSequence) {
        this.container = generateContainer.get();
        this.generateContainer = generateContainer;
        this.generateSequence = generateSequence;
    }

    public static DefaultSequenceDatabase<Integer> newIntegerLinkedListInstance(String fileName, Supplier<Sequence<Integer>> generateSequence) {
        return DefaultSequenceDatabase.loadFromFile(fileName, () -> new LinkedList<>(), generateSequence, Integer::parseInt);
    }

    public static DefaultSequenceDatabase<String> newStringLinkedListInstance(String fileName, Supplier<Sequence<String>> generateSequence) {
        return DefaultSequenceDatabase.loadFromFile(fileName, () -> new LinkedList<>(), generateSequence, s -> s);
    }

    public static <T> DefaultSequenceDatabase<T> loadFromFile(String fileName, Supplier<Collection<SequenceDatabaseRecord<T>>> generateContainer, Supplier<Sequence<T>> generateSequence, Function<String, T> parseItem) {
        DefaultSequenceDatabase<T> database = new DefaultSequenceDatabase<>(generateContainer, generateSequence);
        try (
                FileInputStream fin = new FileInputStream(new File(fileName));
                BufferedReader myInput = new BufferedReader(new InputStreamReader(fin))
        ) {
            String thisLine;
            int id = 0;
            while ((thisLine = myInput.readLine()) != null) {
                if (!thisLine.isEmpty() && thisLine.charAt(0) != '#' && thisLine.charAt(0) != '%' && thisLine.charAt(0) != '@') {
                    database.add(parseRecord(generateSequence.get(), ++id, thisLine.split(" "), parseItem));
                }
            }
        } catch (IOException | ParseException exception) {
            throw new RuntimeException(exception);
        }
        return database;
    }

    private static <T> DefaultSequenceDatabaseRecord<T> parseRecord(Sequence<T> sequence, int id, String[] tokens, Function<String, T> parseItem) throws ParseException {
        for (String token : tokens) {
            // assume each record consists of itemsets with a single item.
            // if the token is -1, it means that we reached the end of an itemset.
            switch (token) {
                case "-1":
                    // ignore
                    break;
                // if the token is -2, it means that we reached the end of the sequence.
                case "-2":
                    return new DefaultSequenceDatabaseRecord<>(id, sequence);
                default:
                    // otherwise it is an item.
                    sequence.add(parseItem.apply(token));
                    break;
            }
        }
        throw new ParseException("no -2", 0);
    }

    @Override
    public SequenceDatabase<T> getProjected(Sequence<T> pattern) {
        SequenceDatabase<T> seqDB = newInstance();
        if (pattern.isEmpty()) {
            seqDB.addAll(container);
            return seqDB;
        }
        int patternSize = pattern.size();
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0 || sequenceSize < patternSize) {
                continue;
            }

            int y = 0;
            for (T value : pattern) {
                for (; y < sequenceSize; ++y) {
                    if (value.equals(sequence.get(y))) {
                        break;
                    }
                }
                if (y++ == sequenceSize) {
                    break;
                }
            }
            --y;

            if (y < sequenceSize) {
                seqDB.addRecord(record.getID(), sequence.getSuffix(sequenceSize - y - 1, false));
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getProjected(T event) {
        SequenceDatabase<T> seqDB = newInstance();
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0) {
                continue;
            }

            for (int y = 0; y < sequenceSize; ++y) {
                if (event.equals(sequence.get(y))) {
                    seqDB.addRecord(record.getID(), sequence.getSuffix(sequenceSize - y - 1, false));
                    break;
                }
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getAllProjected(Sequence<T> pattern, boolean useNewNumber) {
        SequenceDatabase<T> seqDB = newInstance();
        int recordNumber = 0;
        if (pattern.isEmpty()) {
            for (SequenceDatabaseRecord<T> record : container) {
                seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), record.getSequence().getSuffix(record.getSequence().size(), true));
            }
            return seqDB;
        }
        int patternSize = pattern.size();
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0 || sequenceSize < patternSize) {
                continue;
            }

            int y = 0;
            for (T value : pattern) {
                for (; y < sequenceSize; ++y) {
                    if (value.equals(sequence.get(y))) {
                        break;
                    }
                }
                if (y++ == sequenceSize) {
                    break;
                }
            }
            --y;

            for (T last = pattern.get(pattern.size() - 1); y < sequenceSize; ++y) {
                if (last.equals(sequence.get(y))) {
                    seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), sequence.getSuffix(sequenceSize - y - 1, true));
                }
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getAllProjectedWithThis(T event, boolean useNewNumber) {
        SequenceDatabase<T> seqDB = newInstance();
        int recordNumber = 0;
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), sequence.getSuffix(sequenceSize, true));
            if (sequenceSize <= 0) {
                continue;
            }

            for (int y = 0; y < sequenceSize; ++y) {
                if (event.equals(sequence.get(y))) {
                    seqDB.addRecord(useNewNumber ? ++recordNumber : record.getID(), sequence.getSuffix(sequenceSize - y - 1, true));
                }
            }
        }
        return seqDB;
    }

    public SequenceDatabase<T> getSuffixProjected(int index, Sequence<T> pattern) {
        SequenceDatabase<T> seqDB = newInstance();
        if (pattern.isEmpty()) {
            return seqDB;
        }
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            if (sequence.size() <= 0) {
                continue;
            }

            int y = sequence.size() - 1;
            for (int x = pattern.size() - 1; x >= 0; --x) {
                T value = pattern.get(x);
                for (; y >= 0; --y) {
                    if (value.equals(sequence.get(y))) {
                        break;
                    }
                }
                if (y-- < 0) {
                    break;
                }
            }
            ++y;

            T first = pattern.get(0);
            int count = 0;
            for (; y >= 0; --y) {
                if (first.equals(sequence.get(y))) {
                    if (++count == index) {
                        seqDB.addRecord(record.getID(), sequence.getSuffix(sequence.size() - y, false));
                        break;
                    }
                }
            }
        }
        return seqDB;
    }

    public List<SequenceDatabase<T>> getSuffixAllProjected(Sequence<T> pattern) {
        List<SequenceDatabase<T>> sufDB = new ArrayList<>();
        if (pattern.isEmpty()) {
            return sufDB;
        }
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            if (sequence.size() <= 0) {
                continue;
            }

            int y = sequence.size() - 1;
            for (int x = pattern.size() - 1; x >= 0; --x) {
                T value = pattern.get(x);
                for (; y >= 0; --y) {
                    if (value.equals(sequence.get(y))) {
                        break;
                    }
                }
                if (y-- < 0) {
                    break;
                }
            }
            ++y;

            T first = pattern.get(0);
            int index = 0;
            for (; y >= 0; --y) {
                if (first.equals(sequence.get(y))) {
                    if (sufDB.size() <= index) {
                        sufDB.add(newInstance());
                    }
                    SequenceDatabase<T> seqDB = sufDB.get(index);
                    seqDB.addRecord(record.getID(), sequence.getSuffix(sequence.size() - y, false));
                    ++index;
                }
            }
        }
        return sufDB;
    }

    @Override
    public SequenceDatabase<T> getPrefixProjected(T event) {
        SequenceDatabase<T> seqDB = newInstance();
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0) {
                continue;
            }

            for (int y = sequenceSize - 1; y >= 0; --y) {
                if (event.equals(sequence.get(y))) {
                    seqDB.addRecord(record.getID(), sequence.getPrefix(y, false));
                    break;
                }
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> getPrefixProjected(Sequence<T> pattern) {
        SequenceDatabase<T> seqDB = newInstance();
        if (pattern.isEmpty()) {
            seqDB.addAll(container);
            return seqDB;
        }
        int patternSize = pattern.size();
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> sequence = record.getSequence();
            int sequenceSize = sequence.size();
            if (sequenceSize <= 0 || sequenceSize < patternSize) {
                continue;
            }

            int y = sequenceSize - 1;
            for (int x = patternSize - 1; x >= 0; --x) {
                T value = pattern.get(x);
                for (; y >= 0; --y) {
                    if (value.equals(sequence.get(y))) {
                        break;
                    }
                }
                if (y-- < 0) {
                    break;
                }
            }
            ++y;

            if (y >= 0) {
                seqDB.addRecord(record.getID(), sequence.getPrefix(y + 1, false));
            }
        }
        return seqDB;
    }

    @Override
    public SequenceDatabase<T> newInstance() {
        return new DefaultSequenceDatabase<>(generateContainer, generateSequence);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        forEach(record -> builder.append(record.toString()).append("\n"));
        return builder.toString();
    }

    @Override
    public int size() {
        return container.size();
    }

    @Override
    public Iterator<SequenceDatabaseRecord<T>> iterator() {
        return container.iterator();
    }

    @Override
    public boolean add(SequenceDatabaseRecord<T> record) {
        return container.add(record);
    }

    @Override
    public void addRecord(int id, Sequence<T> sequence, Object... params) {
        add(new DefaultSequenceDatabaseRecord<>(id, sequence));
    }

    @Override
    public SequenceDatabase<T> getOptimized(int minSup) {
        Set<T> frequentItems = new HashSet<>(getFrequentItems(minSup));
        SequenceDatabase<T> optimizedSequenceDatabase = newInstance();
        for (SequenceDatabaseRecord<T> record : container) {
            Sequence<T> optimizedSequence = generateSequence.get();
            for (T item : record.getSequence()) {
                if (frequentItems.contains(item)) {
                    optimizedSequence.add(item);
                }
            }
            if (optimizedSequence.size() > 0) {
                optimizedSequenceDatabase.addRecord(record.getID(), optimizedSequence);
            }
        }
        return optimizedSequenceDatabase;
    }
}

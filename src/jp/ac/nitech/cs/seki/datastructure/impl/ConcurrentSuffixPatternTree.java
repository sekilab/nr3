package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.SuffixPatternTree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ConcurrentSuffixPatternTree<T> implements SuffixPatternTree<T> {
    private final SuffixPatternTree<T> parent;
    private final List<SuffixPatternTree<T>> children;
    private final T value;

    private SequenceDatabase<T> projected;

    public ConcurrentSuffixPatternTree(SuffixPatternTree<T> parent, T value, SequenceDatabase<T> projected) {
        this.parent = parent;
        this.value = value;
        this.projected = projected;
        this.children = Collections.synchronizedList(new ArrayList<>());
    }

    @Override
    public SuffixPatternTree<T> getParent() {
        return parent;
    }

    @Override
    public List<SuffixPatternTree<T>> getChildren() {
        return children;
    }

    @Override
    public T getValue() {
        return value;
    }

    @Override
    public SequenceDatabase<T> getProjected() {
        return projected;
    }

    @Override
    public void setProjected(SequenceDatabase<T> projected) {
        this.projected = projected;
    }

    @Override
    public SuffixPatternTree<T> addChild(T value, SequenceDatabase<T> projected) {
        SuffixPatternTree<T> child = new ConcurrentSuffixPatternTree<>(this, value, projected);
        children.add(child);
        return child;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(value);
        if (projected != null) {
            builder.append("(").append(projected.size()).append("):");
        }
        builder.append("[");
        children.forEach(node -> builder.append(node).append(" "));
        builder.append("]");
        return builder.toString();
    }
}

package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.Sequence;

import java.util.List;

public class NR3Rule<T> extends SequenceImpl<T> implements Rule<T> {
    private final Sequence<T> preCondition;
    private final Sequence<T> postCondition;

    private int sequenceSupport;
    private int prefixSequenceSupport;
    private int instanceSupport;
    private double confidence;

    public NR3Rule(List<T> container, Sequence<T> preCondition, Sequence<T> postCondition) {
        super(container);
        this.preCondition = preCondition;
        this.postCondition = postCondition;
        this.addAll(preCondition);
        this.addAll(postCondition);
    }

    public NR3Rule(List<T> container, Sequence<T> preCondition, Sequence<T> postCondition, int sequenceSupport, int prefixSequenceSupport, int instanceSupport, double confidence) {
        this(container, preCondition, postCondition);
        this.sequenceSupport = sequenceSupport;
        this.prefixSequenceSupport = prefixSequenceSupport;
        this.instanceSupport = instanceSupport;
        this.confidence = confidence;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = sequenceSupport;
        result = 31 * result + prefixSequenceSupport;
        result = 31 * result + instanceSupport;
        temp = Double.doubleToLongBits(confidence);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return String.format("%s → %s (%d%s, %d, %s)", preCondition, postCondition, sequenceSupport, prefixSequenceSupport != -1 ? "(" + prefixSequenceSupport + ")" : "", instanceSupport, confidence);
    }

    public Sequence<T> getPreCondition() {
        return preCondition;
    }

    public Sequence<T> getPostCondition() {
        return postCondition;
    }

    public int getSequenceSupport() {
        return sequenceSupport;
    }

    public void setSequenceSupport(int sequenceSupport) {
        this.sequenceSupport = sequenceSupport;
    }

    public int getPrefixSequenceSupport() {
        return prefixSequenceSupport;
    }

    public void setPrefixSequenceSupport(int prefixSequenceSupport) {
        this.prefixSequenceSupport = prefixSequenceSupport;
    }

    public int getInstanceSupport() {
        return instanceSupport;
    }

    public void setInstanceSupport(int instanceSupport) {
        this.instanceSupport = instanceSupport;
    }

    public double getConfidence() {
        return confidence;
    }

    public void setConfidence(double confidence) {
        this.confidence = confidence;
    }
}

package jp.ac.nitech.cs.seki.datastructure.impl;

public class PositionRecord {
    private final int id;
    private final int position;

    public PositionRecord(int id, int position) {
        this.id = id;
        this.position = position;
    }

    public int getID() {
        return id;
    }

    public int getPosition() {
        return position;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d)", id, position);
    }
}

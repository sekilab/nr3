package jp.ac.nitech.cs.seki.datastructure.parallel.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FixedThreadPoolGenerator implements ThreadPoolGenerator {
    private final int threads;

    public FixedThreadPoolGenerator(int threads) {
        this.threads = threads;
    }

    @Override
    public ExecutorService generate() {
        return Executors.newFixedThreadPool(threads);
    }
}

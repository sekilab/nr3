package jp.ac.nitech.cs.seki.datastructure.parallel.pool;

import java.util.concurrent.ExecutorService;

public interface ThreadPoolGenerator {
    ExecutorService generate();
}

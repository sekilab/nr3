package jp.ac.nitech.cs.seki.datastructure.parallel.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

public class ForkJoinPoolGenerator implements ThreadPoolGenerator {
    private final int threads;

    public ForkJoinPoolGenerator(int threads) {
        this.threads = threads;
    }

    @Override
    public ExecutorService generate() {
        return new ForkJoinPool(threads);
    }
}

package jp.ac.nitech.cs.seki.datastructure;

import jp.ac.nitech.cs.seki.datastructure.impl.PartialSequence;
import jp.ac.nitech.cs.seki.datastructure.impl.ReversedSequence;
import jp.ac.nitech.cs.seki.datastructure.impl.SequenceImpl;

import java.util.*;

public interface Sequence<T> extends List<T> {
    static <T> Sequence<T> newLinkedListInstance() {
        return new SequenceImpl<>(new LinkedList<>());
    }

    static <T> Sequence<T> newLinkedListInstance(Sequence<T> sequence) {
        return new SequenceImpl<>(new LinkedList<>(), sequence);
    }

    static <T> Sequence<T> newArrayListInstance() {
        return new SequenceImpl<>(new ArrayList<>());
    }

    static <T> Sequence<T> newArrayListInstance(Sequence<T> sequence) {
        return new SequenceImpl<>(new ArrayList<>(), sequence);
    }

    /**
     * i-th prefix sequence.
     *
     * @param length          length of prefix sequence
     * @param overwriteParent when true, set the parent as returning prefix
     * @return i-th prefix sequence
     */
    default PartialSequence<T> getPrefix(int length, boolean overwriteParent) {
        return new PartialSequence<>(this, 0, length - 1, overwriteParent);
    }

    /**
     * j-th suffix sequence.
     *
     * @param length          length of suffix sequence
     * @param overwriteParent when true, set the parent as returning suffix
     * @return j-th suffix sequence
     */
    default PartialSequence<T> getSuffix(int length, boolean overwriteParent) {
        int size = size();
        return new PartialSequence<>(this, size - length, size - 1, overwriteParent);
    }

    default ReversedSequence<T> getReversed() {
        return new ReversedSequence<>(this);
    }

    default Set<T> getFrequentItems() {
        return new HashSet<>(this);
    }

    default boolean isSubsequenceOf(Sequence<T> other) {
        int otherSize = other.size();
        int y = 0;
        for (T value : this) {
            for (; y < otherSize; ++y) {
                if (value.equals(other.get(y))) {
                    break;
                }
            }
            if (y == otherSize) {
                return false;
            }
            ++y;
        }
        return true;
    }
}

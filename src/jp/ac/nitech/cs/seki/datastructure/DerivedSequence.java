package jp.ac.nitech.cs.seki.datastructure;

public interface DerivedSequence<T> extends Sequence<T> {
    Sequence<T> getOriginalSequence();
}

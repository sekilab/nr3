package jp.ac.nitech.cs.seki.datastructure;

public interface SequenceDatabaseRecord<T> {
    int getID();

    Sequence<T> getSequence();
}

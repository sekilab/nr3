package jp.ac.nitech.cs.seki.datastructure;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class FrequentItemSet<T> implements Set<T> {
    private final HashMap<T, Integer> itemMap;

    public FrequentItemSet() {
        itemMap = new HashMap<>();
    }

    public FrequentItemSet(Collection<T> container) {
        itemMap = new HashMap<>();
        addAll(container);
    }

    public FrequentItemSet(FrequentItemSet<T> frequentItemSet) {
        itemMap = new HashMap<>(frequentItemSet.itemMap);
    }

    public HashMap<T, Integer> getItemMap() {
        return itemMap;
    }

    @Override
    public int size() {
        return itemMap.size();
    }

    @Override
    public boolean isEmpty() {
        return itemMap.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return itemMap.containsKey(o);
    }

    @Override
    public Iterator<T> iterator() {
        return itemMap.keySet().iterator();
    }

    @Override
    public Object[] toArray() {
        return itemMap.keySet().toArray();
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return itemMap.keySet().toArray(a);
    }

    @Override
    public boolean add(T t) {
        Integer count = itemMap.getOrDefault(t, 0);
        itemMap.put(t, count + 1);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        return itemMap.compute((T) o, (key, old) -> (old == null || old == 1) ? null : old - 1) == null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return itemMap.keySet().containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (T value : c) {
            add(value);
        }
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean result = false;
        for (Object value : c) {
            result |= remove(value);
        }
        return result;
    }

    @Override
    public void clear() {
        itemMap.clear();
    }
}

package jp.ac.nitech.cs.seki.datastructure;

public interface Rule<T> {
    Sequence<T> getPreCondition();

    Sequence<T> getPostCondition();
}

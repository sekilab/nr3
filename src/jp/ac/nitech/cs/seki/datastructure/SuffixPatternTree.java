package jp.ac.nitech.cs.seki.datastructure;

import java.util.List;

public interface SuffixPatternTree<T> {
    SuffixPatternTree<T> getParent();

    List<SuffixPatternTree<T>> getChildren();

    T getValue();

    SequenceDatabase<T> getProjected();

    void setProjected(SequenceDatabase<T> projected);

    SuffixPatternTree<T> addChild(T value, SequenceDatabase<T> projected);
}

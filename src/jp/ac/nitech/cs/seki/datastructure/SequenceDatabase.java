package jp.ac.nitech.cs.seki.datastructure;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public interface SequenceDatabase<T> extends Collection<SequenceDatabaseRecord<T>> {
    SequenceDatabase<T> getProjected(Sequence<T> pattern);

    SequenceDatabase<T> getProjected(T event);

    SequenceDatabase<T> getAllProjected(Sequence<T> pattern, boolean useNewNumber);

    SequenceDatabase<T> getAllProjectedWithThis(T event, boolean useNewNumber);

    SequenceDatabase<T> getPrefixProjected(T event);

    SequenceDatabase<T> getPrefixProjected(Sequence<T> pattern);

    SequenceDatabase<T> newInstance();

    SequenceDatabase<T> getOptimized(int minSup);

    default List<T> getFrequentItems(int minSup) {
        FrequentItemSet<T> frequentItemSet = new FrequentItemSet<>();

        for (SequenceDatabaseRecord<T> record : this) {
            frequentItemSet.addAll(record.getSequence().getFrequentItems());
        }

        List<T> frequentItems = new ArrayList<>();
        frequentItemSet.getItemMap().forEach((item, count) -> {
            if (count >= minSup) {
                frequentItems.add(item);
            }
        });
        return frequentItems;
    }

    void addRecord(int id, Sequence<T> sequence, Object... params);
}

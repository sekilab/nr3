package jp.ac.nitech.cs.seki.evaluator;

import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.algorithm.bob.sequential.SequentialBOBMiner;
import jp.ac.nitech.cs.seki.algorithm.ebi.SequentialEBIMiner;
import jp.ac.nitech.cs.seki.algorithm.nr3.parallel.NaiveParallelNR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.parallel.ParallelLoopFusedNR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.parallel.PipelineNR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialEnhancedNR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialLoopFusedNR3Miner;
import jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialLoopFusedNR3RuleGenerator;
import jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialNR3Miner;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;
import jp.ac.nitech.cs.seki.datastructure.parallel.pool.FixedThreadPoolGenerator;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;

public class EvaluatorRunner<T> {
    private final boolean logConsole;
    private final boolean logRules;
    private final boolean logMaxMemory;

    private final int[] threadCount = new int[]{8, 6, 4, 2};
    private Supplier<String> filenameSupplier = () -> "EvaluationResult-" + new SimpleDateFormat("yyMMdd.HHmmss.SSS").format(new Date().getTime()) + ".log";

    private boolean useEBISequential = false;
    private boolean useBOBSequential = false;
    private boolean useNR3Enhanced = false;
    private boolean useNR3ParallelLoopFused = false;
    private boolean useNR3Pipeline = false;
    private boolean useNR3Naive = false;
    private boolean useNR3Sequential = false;
    private boolean useNR3SequentialLoopFused = false;

    public EvaluatorRunner(boolean logConsole, boolean logRules, boolean logMaxMemory, Supplier<String> filenameSupplier) {
        this(logConsole, logRules, logMaxMemory);
        this.filenameSupplier = filenameSupplier;
    }

    public EvaluatorRunner(boolean logConsole, boolean logRules, boolean logMaxMemory) {
        this.logConsole = logConsole;
        this.logRules = logRules;
        this.logMaxMemory = logMaxMemory;
    }

    public int[] getThreadCount() {
        return threadCount;
    }

    public boolean isUsingEBISequential() {
        return useEBISequential;
    }

    public void useEBISequential(boolean useEBISequential) {
        this.useEBISequential = useEBISequential;
    }

    public boolean isUsingBOBSequential() {
        return useEBISequential;
    }

    public void useBOBSequential(boolean useBOBSequential) {
        this.useBOBSequential = useBOBSequential;
    }

    public boolean isUsingNR3Enhanced() {
        return useNR3Enhanced;
    }

    public void useNR3Enhanced(boolean useNR3Enhanced) {
        this.useNR3Enhanced = useNR3Enhanced;
    }

    public boolean isUsingNR3ParallelLoopFused() {
        return useNR3ParallelLoopFused;
    }

    public void useNR3ParallelLoopFused(boolean useNR3ParallelLoopFused) {
        this.useNR3ParallelLoopFused = useNR3ParallelLoopFused;
    }

    public boolean isUsingNR3Pipeline() {
        return useNR3Pipeline;
    }

    public void useNR3Pipeline(boolean useNR3Pipeline) {
        this.useNR3Pipeline = useNR3Pipeline;
    }

    public boolean isUsingNR3Naive() {
        return useNR3Naive;
    }

    public void useNR3Naive(boolean useNR3Naive) {
        this.useNR3Naive = useNR3Naive;
    }

    public boolean isUsingNR3Sequential() {
        return useNR3Sequential;
    }

    public void useNR3Sequential(boolean useNR3Sequential) {
        this.useNR3Sequential = useNR3Sequential;
    }

    public boolean isUsingNR3SequentialLoopFused() {
        return useNR3SequentialLoopFused;
    }

    public void useNR3SequentialLoopFused(boolean useNR3SequentialLoopFused) {
        this.useNR3SequentialLoopFused = useNR3SequentialLoopFused;
    }

    public Map<String, List<EvaluationResult<T>>> run(SequenceDatabase<T> database, List<List<Object>> evaluationParameters) {
        List<Miner> miners = getMiners();

        try (PrintStream writeStream = logConsole ? System.out : new PrintStream(filenameSupplier.get(), "utf-8")) {
            return Evaluator.evaluateAll(writeStream, database, miners, evaluationParameters, logRules, logMaxMemory);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private List<Miner> getMiners() {
        List<Miner> miners = new ArrayList<>();

        if (useEBISequential) {
            miners.add(new SequentialEBIMiner<>("SequentialEBI"));
        }

        if (useBOBSequential) {
            miners.add(new SequentialBOBMiner<>("SequentialBOB"));
        }

        if (useNR3Enhanced) {
            miners.add(new SequentialEnhancedNR3Miner("SequentialEnhancedNR3"));
        }

        if (useNR3ParallelLoopFused) {
            for (int count : threadCount) {
                String name = "ParallelLoopFusedNR3-FixedThreadPool-" + count;
                miners.add(new ParallelLoopFusedNR3Miner(name, new FixedThreadPoolGenerator(count)));
            }
        }

        if (useNR3Pipeline) {
            for (int count : threadCount) {
                String name = "PipelineNR3-FixedThreadPool-" + count;
                miners.add(new PipelineNR3Miner<>(name, new FixedThreadPoolGenerator(count)));
            }
        }

        if (useNR3Naive) {
            for (int count : threadCount) {
                String name = "NaiveParallelNR3-FixedThreadPool-" + count;
                miners.add(new NaiveParallelNR3Miner<>(name, new FixedThreadPoolGenerator(count)));
            }
        }

        if (useNR3SequentialLoopFused) {
            miners.add(new SequentialLoopFusedNR3Miner<>("SequentialLoopFusedNR3", SequentialLoopFusedNR3RuleGenerator.getInstance()));
        }

        if (useNR3Sequential) {
            miners.add(new SequentialNR3Miner<>("SequentialNR3"));
        }

        return miners;
    }
}

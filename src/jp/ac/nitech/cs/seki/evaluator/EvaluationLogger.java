package jp.ac.nitech.cs.seki.evaluator;

import jp.ac.nitech.cs.seki.algorithm.EvaluationParameter;
import jp.ac.nitech.cs.seki.datastructure.Rule;

import java.io.PrintStream;
import java.util.Date;
import java.util.List;

public class EvaluationLogger<T> {
    private final PrintStream writeStream;
    private final EvaluationParameter parameter;
    private final EvaluationResult<T> result;

    public EvaluationLogger(String minerName, EvaluationParameter parameter) {
        this(minerName, parameter, System.out);
    }

    public EvaluationLogger(String minerName, EvaluationParameter parameter, PrintStream writeStream) {
        this.writeStream = writeStream;
        this.parameter = parameter;
        result = new EvaluationResult<>(minerName, parameter);
    }

    public PrintStream getWriteStream() {
        return writeStream;
    }

    public EvaluationParameter getParameter() {
        return parameter;
    }

    public EvaluationResult<T> getResult() {
        return result;
    }

    public Object addMetadata(String name, Object value) {
        return result.getMetadata().put(name, value);
    }

    public Date logTimestamp(String name) {
        return result.getTimestamps().put(name, new Date());
    }

    public void print(Object object) {
        writeStream.print(object);
    }

    public void println() {
        writeStream.println();
    }

    public void println(Object object) {
        writeStream.println(object);
    }

    public void setRules(List<Rule<T>> rules) {
        result.setRules(rules);
    }
}

package jp.ac.nitech.cs.seki.evaluator;

import com.sun.management.GarbageCollectionNotificationInfo;

import javax.management.NotificationEmitter;
import javax.management.NotificationListener;
import javax.management.openmbean.CompositeData;
import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;

public class MemoryLogger {
    private boolean init = false;
    private boolean started = false;
    private long maxMemory = -1;

    private final NotificationListener listener = (notification, handback) -> {
        if (started) {
            if (notification.getType().equals(GarbageCollectionNotificationInfo.GARBAGE_COLLECTION_NOTIFICATION)) {
                GarbageCollectionNotificationInfo info = GarbageCollectionNotificationInfo.from((CompositeData) notification.getUserData());
                // refreshMaxMemory(info.getGcInfo().getMemoryUsageBeforeGc().values().stream().mapToLong(mem -> mem.getUsed()).sum());
                refreshMaxMemory(info.getGcInfo().getMemoryUsageAfterGc().values().stream().mapToLong(mem -> mem.getUsed()).sum());
            }
        }
    };

    private void refreshMaxMemory(long size) {
        if (maxMemory < size) {
            maxMemory = size;
        }
    }

    public void start() {
        if (!init) {
            init = true;
            for (GarbageCollectorMXBean gcBean : ManagementFactory.getGarbageCollectorMXBeans()) {
                NotificationEmitter emitter = (NotificationEmitter) gcBean;
                emitter.addNotificationListener(listener, null, null);
            }
        }
        maxMemory = -1;
        started = true;
    }

    public void finish() {
        started = false;
    }

    public long getMaxMemory() {
        return maxMemory;
    }
}

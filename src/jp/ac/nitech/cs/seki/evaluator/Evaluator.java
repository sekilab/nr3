package jp.ac.nitech.cs.seki.evaluator;

import jp.ac.nitech.cs.seki.algorithm.EvaluationParameter;
import jp.ac.nitech.cs.seki.algorithm.Miner;
import jp.ac.nitech.cs.seki.datastructure.Rule;
import jp.ac.nitech.cs.seki.datastructure.SequenceDatabase;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.*;

public class Evaluator {
    private static final SimpleDateFormat dateFormatter = new SimpleDateFormat("yyMMdd.HHmmss.SSS");

    public static <T, P extends EvaluationParameter> EvaluationResult<T> evaluate(PrintStream writeStream, SequenceDatabase<T> sequenceDatabase, Miner<T, P> miner, P parameter, boolean logRules) {
        EvaluationLogger<T> logger = new EvaluationLogger<>(miner.getName(), parameter, writeStream);

        System.out.println("Evaluating " + miner.getName() + " at " + new Date().getTime());
        logger.logTimestamp("Mining started");

        List<Rule<T>> minedRules = miner.mine(sequenceDatabase, parameter, logger);

        logger.logTimestamp("Mining finished");
        System.out.println("Finished " + miner.getName() + " at " + new Date().getTime());

        logger.addMetadata("Mined Rules", minedRules.size());
        if (logRules) {
            logger.setRules(minedRules);
        }

        return logger.getResult();
    }

    public static <T> Map<String, List<EvaluationResult<T>>> evaluateAll(PrintStream writeStream, SequenceDatabase<T> sequenceDatabase, List<Miner> evaluationPairs, List<List<Object>> parameters, boolean logRules, boolean logMaxMemory) {
        Map<String, List<EvaluationResult<T>>> resultMap = new TreeMap<>();
        MemoryLogger memoryLogger = new MemoryLogger();

        writeStream.println("EvaluationResult-" + (dateFormatter.format(new Date())));
        writeStream.println("========================================");

        int databaseSize = sequenceDatabase.size();
        for (List<Object> parameter : parameters) {
            System.out.println("=================================================");
            System.out.println("Evaluating Parameter " + parameter);
            writeStream.println("Parameter: " + parameter);
            Object[] params = parameter.toArray();
            for (Miner miner : evaluationPairs) {
                System.out.println("-------------------------------------------------");
                for (int i = 0; i < 3200; ++i) {
                    List<Integer> dummyList = new ArrayList<>();
                    dummyList.add(i);
                }
                System.gc();
                sleepThread(5000);
                if (logMaxMemory) {
                    memoryLogger.start();
                }
                EvaluationResult<T> result = evaluate(writeStream, sequenceDatabase, miner, miner.getParameter(databaseSize, params), logRules);
                if (logMaxMemory) {
                    memoryLogger.finish();
                    result.setMaxMemory(memoryLogger.getMaxMemory());
                }
                resultMap.computeIfAbsent(miner.getName(), k -> new ArrayList<>()).add(result);
                writeResult(writeStream, result);
            }
            writeStream.println();
            writeStream.println("========================================");
        }
        return resultMap;
    }

    private static void sleepThread(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            // Do nothing.
            e.printStackTrace();
        }
    }

    private static <T> void writeResult(PrintStream writeStream, EvaluationResult<T> result) {
        writeStream.println("----------------------------------------");
        writeStream.println("Miner: " + result.getMinerName());
        writeStream.println("Parameter: " + result.getParameters());
        writeStream.println("Timestamps:");
        Map<String, Date> timestamps = result.getTimestamps();
        Date previous = null;
        for (Map.Entry<String, Date> entry : timestamps.entrySet()) {
            String name = entry.getKey();
            Date date = entry.getValue();

            writeStream.printf("  - %s\t\t%d%s%n", name, date.getTime(), previous != null ? " (" + (date.getTime() - previous.getTime()) + ")" : "");
            previous = date;
        }
        writeStream.println("  * Elapsed(ms): " + (timestamps.get("Mining finished").getTime() - timestamps.get("Mining started").getTime()));
        writeStream.println();
        if (result.getMaxMemory() != -1) {
            writeStream.printf("Maximum Used Memory: %.3f MB", result.getMaxMemory() / 1024.0 / 1024.0);
            writeStream.println();
        }
        writeStream.println("Metadata:");
        result.getMetadata().forEach((name, value) -> writeStream.println("  - " + name + " = " + value));
        if (result.getRules() != null) {
            writeStream.println();
            writeStream.println("Mined Rules: " + result.getRules().size());
            result.getRules().forEach(writeStream::println);
        }
    }
}

package jp.ac.nitech.cs.seki.evaluator;

import jp.ac.nitech.cs.seki.algorithm.EvaluationParameter;
import jp.ac.nitech.cs.seki.datastructure.Rule;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class EvaluationResult<T> {
    private final String minerName;
    private final Map<String, Date> timestamps;
    private final Map<String, Object> metadata;
    private final EvaluationParameter parameters;
    private long maxMemory;
    private List<Rule<T>> rules;

    public EvaluationResult(String minerName, EvaluationParameter parameter) {
        this.minerName = minerName;
        this.parameters = parameter;
        this.timestamps = new LinkedHashMap<>();
        this.metadata = new LinkedHashMap<>();
        this.rules = null;
        this.maxMemory = -1;
    }

    public String getMinerName() {
        return minerName;
    }

    public Map<String, Date> getTimestamps() {
        return timestamps;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public EvaluationParameter getParameters() {
        return parameters;
    }

    public List<Rule<T>> getRules() {
        return rules;
    }

    public void setRules(List<Rule<T>> rules) {
        this.rules = rules;
    }

    public long getMaxMemory() {
        return maxMemory;
    }

    public void setMaxMemory(long maxMemory) {
        this.maxMemory = maxMemory;
    }
}

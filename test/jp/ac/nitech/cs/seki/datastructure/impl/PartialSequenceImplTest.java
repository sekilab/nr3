package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Sequence;

import java.util.Arrays;

public class PartialSequenceImplTest {

    private static void testFromSequence(Sequence<Integer> originalSequence) {
        { // Implemented methods
            PartialSequence<Integer> sequence = new PartialSequence<>(originalSequence, 2, 5);

            { //
            }
        }

        { // Inherited methods

        }
    }

    public static void main(String[] args) {
        Sequence<Integer> originalSequence = Sequence.newArrayListInstance();
        originalSequence.addAll(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));

        testFromSequence(originalSequence);
    }
}

package jp.ac.nitech.cs.seki.datastructure.impl;

import jp.ac.nitech.cs.seki.datastructure.Sequence;

import java.util.Arrays;

public class ReversedSequenceImplTest {

    private static void testFromSequence(Sequence<Integer> originalSequence) {
        ReversedSequence<Integer> sequence = new ReversedSequence<>(originalSequence);

        { // Implemented methods
            { // get(int)
                if (sequence.get(3) != 5 || sequence.get(0) != 8 || sequence.get(8) != 0) {
                    throw new RuntimeException("sequence.get(3) != 5 || sequence.get(0) != 8 || sequence.get(8) != 0");
                }
            }
        }

        { // Inherited methods
            { // getPrefix(int)
                PartialSequence<Integer> prefix = sequence.getPrefix(4, false);

                if (prefix.get(1) != 7 || prefix.get(0) != 8 || prefix.get(3) != 5) {
                    throw new RuntimeException("prefix.get(1) != 7 || prefix.get(0) != 8 || prefix.get(3) != 5");
                }
            }

            { // getRevered()
                ReversedSequence<Integer> revered = sequence.getReversed();

                if (revered.get(3) != 3 || revered.get(0) != 0 || revered.get(8) != 8) {
                    throw new RuntimeException("sequence.get(3) != 3 || sequence.get(0) != 0 || sequence.get(8) != 8");
                }
            }

            { // getSuffix(int)
                PartialSequence<Integer> suffix = sequence.getSuffix(4, false);

                if (suffix.get(1) != 2 || suffix.get(0) != 3 || suffix.get(3) != 0) {
                    throw new RuntimeException("suffix.get(1) != 2 || suffix.get(0) != 3 || sequence.get(3) != 0");
                }
            }
        }
    }

    public static void main(String[] args) {
        Sequence<Integer> originalSequence = Sequence.newArrayListInstance();
        originalSequence.addAll(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8));

        testFromSequence(originalSequence);
    }
}

Recurrent Rules Miners
======================

This repository contains implementations of several recurrent rules miners.


## Implemented Miners

- Non-redundant recurrent rules miner (NR3) - (Lo et al., 2008)

    ``` jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialNR3Miner ```

- Naive parallel non-redundant recurrent rules miner

    ``` jp.ac.nitech.cs.seki.algorithm.nr3.parallel.NaiveParallelNR3Miner ```

- Parallel non-redundant recurrent rules miner (PNR3) - (Yoon et al., 2017)

    ``` jp.ac.nitech.cs.seki.algorithm.nr3.parallel.PipelineNR3Miner ```

- Loop-Fused non-redundant recurrent rules miner (LF-NR3) - (Yoon et al., 2017)

    ``` jp.ac.nitech.cs.seki.algorithm.nr3.sequential.SequentialLoopFusedNR3Miner ```

- Bidirectional pruning-based recurrent rules miner (BOB) - (Lo et al., 2011)

    ``` jp.ac.nitech.cs.seki.algorithm.bob.algorithm.sequential.SequentialBOBMiner ```

\documentclass{llncs}
%
% \usepackage{makeidx}  % allows for indexgeneration
\usepackage{latexsym}
\usepackage[ruled]{algorithm2e}
\usepackage[pdftex]{graphicx}
\usepackage{float}
\usepackage{amssymb}
\usepackage{textcmds}
%\pagestyle{plain}
%
\begin{document}
%
\mainmatter              % start of the contributions
%
\title{Parallel Mining of Non-Redundant Recurrent Rules from a Sequence Database}
%
\author{SeungYong Yoon \and Hirohisa Seki}
%
\institute{Department of Computer Science, Nagoya Institute of Technology, \\
Showa-ku, Nagoya 466-8555, Japan\\
\email{24115172@stn.nitech.ac.jp}, \email{seki@nitech.ac.jp}}

\maketitle              % typeset the title of the contribution

\begin{abstract}
Nowadays, there have been proposed several methods for mining sequential patterns from a sequence database.
Among them, recurrent rules, proposed by Lo \textit{et al.}, can express \qq{Whenever a series of precedent events occurs, eventually a series of consequent events occurs}, and the usefulness of recurrent rules has been shown in various domains, including software specification and verification.
Although some algorithms such as \textit{NR$^3$} have been proposed, mining non-redundant recurrent rules still requires considerable processing time.
To reduce the computation cost, we propose a parallel algorithm for mining non-redundant recurrent rules, which fully utilizes the task-parallelism in \textit{NR$^3$}.
We also give some experimental results, which show the effectiveness of our proposed method.

\keywords{data mining, sequential rule, recurrent rule, sequence database, parallel algorithm}
\end{abstract}
%
\section{Introduction}
There are many kinds of data sources in sequential forms such as logs, transaction histories, medical histories, program traces, and many more.
To extract useful information from these sources, many efficient methods have been studied \cite{han2007frequent,mooney2013sequential} since the introduction of the problem of discovering sequential patterns \cite{agrawal1995mining}.

As one of those methods, {\em recurrent rules}, proposed by Lo \textit{et al.}, can express \qq{Whenever a series of events occurs, eventually another series of events occurs} \cite{lo2008efficient}.
Unlike the other sequential rules, recurrent rules capture temporal constraints that repeat a meaningful number of times {\em both within a sequence and across multiple sequences}.
The usefulness of recurrent rules has been shown in various domains such as software specification and verification \cite{wang2013temporal,lo2008mining}.
Lo \textit{et al.} have proposed two mining methods for recurrent rules: \textit{NR$^3$} \cite{lo2008efficient} and \textit{BOB} \cite{lo2011bidirectional}.
Those algorithms adopt some pruning techniques, called {\em rule redundancy}, to mine only meaningful rules, and also to reduce processing time.
Despite these efforts, mining non-redundant recurrent rules still requires considerable processing time.

To reduce the computation cost, we propose a parallel algorithm for mining non-redundant recurrent rules, which aims at fully using the task parallelism in the sequential (or serial) mining algorithms.
We present our parallel approach based on \textit{NR$^3$}, one of the sequential algorithms for mining recurrent rules.

There have been proposed some methods on mining sequential patterns in parallel, \textit{e.g.}, Par-CSP \cite{cong2005parallel}, pDBV-SPM \cite{huynh2016efficient}, pSPADE \cite{zaki2001parallel}, to mention a few.
Our approach focuses on the task parallelism in mining recurrent rules, thus orthogonal to those methods.
To the best of our knowledge, there has been no approach to parallelizing recurrent rule mining as well as the other sequential rule mining.

The outline of this paper is as follows.
In Section 2, we present some preliminaries, and explain Non-Redundant Recurrent Rules Miner (\textit{abbr. NR$^3$}) algorithm by Lo \textit{et al.} \cite{lo2008efficient}.
Section 3 describes our parallel algorithm, Parallel Non-Redundant Recurrent Rules Miner (\textit{abbr. pNR$^3$}), and discusses some implementation issues.
Section 4 presents some experimental results on a synthetic and real dataset to show the effectiveness of our algorithm.
Finally, we conclude and state future works in Section 5.

\section{Generating Recurrent Rules}
In this section, we present some preliminaries and definitions on mining non-redundant recurrent rules, mainly taken from \cite{lo2011bidirectional}, and the \textit{NR$^3$} algorithm proposed by Lo \textit{et al.} \cite{lo2008efficient}.

\subsection{Preliminaries}

\subsubsection{Basic Notations}
Let $I$ be a set of distinct \textit{events} considered.
We define a \textit{sequence} $S$ as an ordered list of $n$ events, denoted as $S = \left<e_1, e_2, ..., e_n\right>$ where $S[i] = e_i \in I$, while $i$ of each $e_i$ in $S$ is called as a \textit{temporal point}, $n$ is called as the \textit{length} of $S$, and the \textit{j-prefix of S} (\textit{i.e.} $S^j$) as $\left<e_1, e_2, ..., e_j\right>$ for a given temporal point $j$.
A \textit{sequence database} $SeqDB$ is defined as a set of $N$ sequences, denoted as $SeqDB = \{S_1, S_2, ..., S_N\}$, while we call $N$ as the \textit{size} of $SeqDB$ (\textit{i.e.} $\vert SeqDB \vert$).
A \textit{pattern} $P$ is a series of events, and $last(P)$ denotes the last event of $P$.
Consider two patterns $P_1 = \left<e_1, e_2, ..., e_n\right>$ and $P_2 = \left<e'_1, e'_2, ..., e'_m \right>$.
A pattern $P_1 +\!\!+ P_2$ denotes the concatenation of patterns $P_1$ and $P_2$.
We call $P_1$ is a \textit{super-sequence} of $P_2$ if $e_{i_1} = e'_1, e_{i_2} = e'_2, ..., e_{i_m} = e'_m (1 \leq i_1 \leq ... \leq i_m \leq n)$ and denote $P_1 \sqsupseteq P_2$.
Given a pattern $P$, a sequence $S$ and a temporal point $j$ s.t. $S^j \sqsupseteq P$ and $last(P) = S[j]$, $S^j$ is an \textit{instance} of $P$ in $S$ while the shortest instance is called as the \textit{minimum} (\textit{i.e.}, $\nexists k < j$ s.t. $S^k$ is an instance of $P$).

\vspace{4pt}

We show an example of a sequence database $SeqDB$ in Table 1, containing two sequences $S_1$ and $S_2$, where $S_2 \sqsupseteq S_1$ and $(S_2)^8$ is the minimum of $S_1$ in $S_2$.

\begin{table}[H]
	\vspace{-10pt}
	\caption{a sequence database $SeqDB$}
	\begin{center}
		\begin{tabular}{|l|l|}
			\hline
			\textbf{Seq ID.}&\textbf{Sequence}\\
			\hline\hline
			$S_1$ & $\left<{\tt check}, {\tt lock}, {\tt use}, {\tt use}, {\tt unlock}\right>$ \\
			$S_2$  & $\left<{\tt check}, {\tt lock}, {\tt use}, {\tt unlock}, {\tt lock}, {\tt use}, {\tt use}, {\tt unlock}, {\tt lock}, {\tt lock}, {\tt exit}\right>$ \\
			\hline
		\end{tabular}
	\end{center}
	\vspace{-10pt}
\end{table}

\subsubsection{Concepts and Problem Statement}
The input of our mining problem is a sequence database, and the output is a set of recurrent rules.
Each recurrent rule we mine expresses: \qq{Whenever a series of events has just occurred at a point in time (\textit{i.e.} a temporal point), eventually another series of events occurs.}
To generate recurrent rules, we need to \textit{peek} at interesting temporal points and \textit{see} what series of events are likely to occur next.

\begin{definition}[Projection \& All-Projection \& Support]
Consider a sequence database $SeqDB$ and a pattern $P$.
$SeqDB$ {\em projected} on $P$ (i.e. $SeqDB_P$) is $\{ (i,sx) \; \vert \; S_i = px+\!\!+sx \in SeqDB$ where $px$ is the minimum instance of $P \}$ while the {\em sequence support} $sup(P, SeqDB)$ is $\vert SeqDB_P \vert$.
$SeqDB$ {\em all-projected} on $P$ (i.e. $SeqDB^{all}_P$) is $\{ (i,sx) \; \vert \; S_i = px+\!\!+sx \in SeqDB$ where $px$ is an instance of $P \}$ while the {\em instance support} $sup^{all}(P,$ $SeqDB)$ is $\vert SeqDB^{all}_P \vert$. $\blacksquare$
\end{definition}

\begin{definition}[Recurrent Rule]
We define a {\em recurrent rule} (or rule) as $R = R_{pre} \to R_{post}$ where $R_{pre}$({\em pre-condition}) and $R_{post}$({\em post-condition}) are two patterns.
For a rule $R$ in a sequence database $SeqDB$, the {\em sequence support}, {\em instance support} and {\em confidence} of $R$ are defined as respectively:
\begin{eqnarray*}
sup(R, SeqDB) & = & sup(R_{pre} +\!\!+ R_{post}, SeqDB) \\
sup^{all}(R, SeqDB) & = & sup^{all}(R_{pre} +\!\!+ R_{post}, SeqDB) \\
conf(R, SeqDB) & = & sup(R_{post}, SeqDB^{all}_{R_{pre}}) / sup^{all}(R_{pre}, SeqDB)
\end{eqnarray*}
A rule $R$ in $SeqDB$ is said to be {\em significant} if its supports and confidence satisfy given thresholds, i.e., $sup(R, SeqDB) \geq min \_ sup$, $sup^{all}(R, SeqDB) \geq min \_ sup^{all}$, and $conf(R, SeqDB) \geq min \_ conf$. $\blacksquare$
\end{definition}

For example, consider again $SeqDB$ in Table 1, and let $R$ be a rule in the form $\left<{\tt lock}\right> \to \left<{\tt unlock}\right>$.
Then, the sequence (instance) support of $R$ is 2 (3), respectively, and the confidence of $R$ is 0.6, since $sup^{all}(\left<{\tt lock}\right>, SeqDB) = 5$, $sup(\left<{\tt unlock}\right>, SeqDB^{all}_{\left<{\tt lock}\right>}) = 3$.

\subsubsection{Mining Non-Redundant Recurrent Rules}
Lo \textit{et al.} give the notion of rule redundancy: a rule $R$ is rendered {\em redundant} by another rule $R'$ if $\exists R' \sqsupseteq R$ s.t. $R$ and $R'$ both have the same support and confidence values, in brief \cite{lo2008efficient}.
Our goal is to find all significant and non-redundant rules in a sequence database.
We generate these rules using some pruning techniques, which are performed on generating pre/post-conditions, proposed by Lo \textit{et al.} \cite{lo2008efficient}.

\subsection{Non-Redundant Recurrent Rules Miner}
We explain the procedure of Non-Redundant Recurrent Rules Miner (\textit{abbr.} \textit{NR$^3$}) proposed in \cite{lo2008efficient} in Algorithm 1.
\textit{NR$^3$} mines a set of significant and non-redundant recurrent rules from a given sequence database $SeqDB$ using 3 specified thresholds $min\_sup$, $min\_sup^{all}$, and $min\_conf$.

The process of \textit{NR$^3$} consists of 3 steps.
In the first step, \textit{NR$^3$} algorithm generates \textit{PreCond}, a pruned set of pre-conditions satisfying $min\_sup$ from $SeqDB$ (line 1), and we call this task \texttt{GenPre}.
The next step is traversing a loop of \texttt{GenRule} tasks (line 2-10, specifically 3-9 for a \texttt{GenRule} task) for each pre-condition $pre \in PreCond$ to generate significant rules $pre \to post$ where $post \in PostCond$ while $PostCond$ is a pruned set of post-conditions from $SeqDB^{all}_{pre}$ satisfying $min\_conf \times \vert SeqDB^{all}_{pre} \vert$.
In the \texttt{GenRule} task, generated rules are stored in the rule candidates $Rules$.
Finally, we remove any remaining redundant recurrent rules in $Rules$, and we call this task \texttt{RemRedun}.
\vspace{-10pt}
\begin{algorithm}[ht]
\caption{Non-Redundant Recurrent Rules Miner (\textit{NR$^3$})}
\DontPrintSemicolon
\LinesNumbered
\KwIn{Database: $SeqDB$; Thresholds: $min\_ sup$, $min\_ sup^{all}$, $min\_ conf$}
\KwOut{Significant and non-redundant recurrent rules}
$PreCond$ := A pruned set of pre-conditions from $SeqDB$ satisfying $min\_sup$\;
\ForEach{$pre \in PreCond$}{
	$SeqDB^{all}_{pre}$ := $SeqDB$ all-projected on $pre$\;
	$PostCond$ := A pruned set of post-conditions from $SeqDB^{all}_{pre}$ satisfying $min\_ conf \times \vert SeqDB^{all}_{pre} \vert$\;
	\ForEach{$post \in PostCond$}{
		\If{$sup^{all}(pre +\!\!+ post) \geq min\_ sup^{all}$}{
			$Rules = Rules \cup \left\{pre \to post\right\}$\;
		}
	}
}
Remove redundancy in $Rules$\;
\Return{$Rules$}
\end{algorithm}
\vspace{-20pt}

\section{Parallel Approach to Mining Recurrent Rules}
Mining recurrent rules requires considerable computation cost compared with the other sequential rule mining methods.
Therefore, we propose a parallel algorithm for mining non-redundant recurrent rules, which can be run on a PC with a multi-core processor.

In this section, we first present our parallel method on mining non-redundant recurrent rules based on \textit{NR$^3$}, and then some implementation issues.

\subsubsection{Parallelizing \textit{NR$^3$} Algorithm}
We fully use the task-parallelism underlying in the \textit{NR$^3$} algorithm, which can be handled within the {\em single-producer-multiple-consumer} framework.
We first use the {\em loop-level parallelism} in the loop of \texttt{GenRule} tasks, since each \texttt{GenRule} task can be performed independently.
Next, we use the {\em task concurrency} between the \texttt{GenPre} task and the \texttt{GenRule} task, since the procedures of both tasks are independent of each others.

Applying the above parallelizations, we now present our algorithm, Parallel Non-Redundant Recurrent Rules Miner (\textit{abbr.} \textit{pNR$^3$}), in Figure 1.
The algorithm first initializes a thread pool with a task queue and a specified number of worker threads, adds a \texttt{GenPre} task into the pool, and runs the pool.
Whenever the \texttt{GenPre} task finds a pre-condition candidate $pre$, it creates a \texttt{GenRule} task using $pre$ and pushes the new task into the task queue of the thread pool.
A \texttt{GenRule} task running in the worker thread will generate significant rules and stores the rules into $Rules$, a list of significant rules.
Once the thread pool becomes empty (\textit{i.e.}, the \texttt{GenPre} task and all of \texttt{GenRule} tasks are finished), the algorithm runs a \texttt{RemRedun} task to derive significant and non-redundant recurrent rules from $Rules$.

\begin{figure}[ht]
	\vspace{-20pt}
	\centering
	\caption{Parallel Non-Redundant Recurrent Rules Miner (\textit{pNR$^3$})}
	\includegraphics[width=12.2cm]{pNR3.pdf}
	\vspace{-40pt}
\end{figure}

\subsubsection{Parallelization Effects of \textit{pNR$^3$}}
Let $N$ be the number of available threads, and $t_{\tt{T}}$ be the runtime of a task \texttt{T}.
The execution time of \textit{NR$^3$} is expressed as $t_{\tt{GenPre}} + t_{\tt{GenRule}} + t_{\tt{RemRedun}}$.
We have used two parallelizations; (a) the loop-level parallelism in the loop of \texttt{GenRule} tasks and (b) the task concurrency between the \texttt{GenPre} task and the \texttt{GenRule} task.
We first estimate the execution time of each parallelizations, ignoring some overhead due to implementation of concurrency.
The runtime of the loop-level parallelism is estimated to $t_{\tt{GenPre}} + t_{\tt{GenRule}} / N + t_{\tt{RemRedun}}$, while that of the task concurrency is estimated to $\max(t_{\tt{GenPre}}, t_{\tt{GenRule}}) + t_{\tt{RemRedun}}$ in optimal cases, and the same as \textit{NR$^3$} in the worst cases.
Since \textit{pNR$^3$} incorporates both parallelizations, the estimated execution time is $\max(t_{\tt{GenPre}}, t_{\tt{GenRule}} / N) + t_{\tt{RemRedun}}$ in optimal cases, or the same as the loop-level parallelism in the worst cases.
We will show our experimental results in the following section to verify the discussion.

\subsubsection{Implementation Issues}
We have implemented our \textit{pNR$^3$} in Java SE 8.
A thread pool of our miner has been implemented with \texttt{FixedThreadPool} which runs a fixed number of threads with a shared unbounded task queue.
In order to store significant rules across concurrent \texttt{GenRule} tasks, $Rules$, a list of significant rules, has been implemented with \texttt{ConcurrentLinkedQueue}, a non-blocking concurrent queue.
To remove redundant rules, \texttt{RemRedun} task has used a hash table which hashes a rule using its supports and confidence.

\section{Experimental Results}
In this section, we present the effectiveness of our algorithm \textit{pNR$^3$} based on some experimental results using synthetic and real datasets.

We have used (i) D10C10N10R0.5 synthetic dataset, which contains 9,678 sequences whose average length is 31.22, generated by the IBM synthetic data generator used in \cite{agrawal1995mining}, and (ii) BMSWebView1 real dataset, which contains 59,601 sequences whose average length is 2.42, provided by SPMF \cite{fournier2014spmf}, containing real click stream data from KDDCup2000 \cite{kohavi2000kdd}.

The experiments have been performed on a PC with an Intel Core i7-3610QM 2.30GHz CPU with 4 physical and 8 logical cores, and 8GB of RAM, running 64-bit Windows 7 Professional, using the algorithms implemented in Java SE 8.
We show the performances of \textit{NR$^3$} (\textit{i.e.}, executed on a single thread) and \textit{pNR$^3$} executed on 2, 4 and 8 threads for two datasets.

\subsubsection{D10C10N10R0.5}
We present experiment results of the D10C10N10R0.5 synthetic dataset in Figure 2.

We plot some results in Figure 2 (a)-(c) when varying $min\_sup$ from 0.5\% to 0.9\% with fixed $min\_conf = 50\%$ and $min\_sup^{all} = 1$;
(a) the total execution times of \textit{NR$^3$} and \textit{pNR$^3$} on 2, 4 and 8 threads,
(b) the ratios of the execution time of the tasks (a \texttt{GenPre} task, \texttt{GenRule} tasks and a \texttt{RemRedun} task) of \textit{NR$^3$},
(c) the sizes of $PreCond$ (\textit{i.e.}, the number of \texttt{GenRule} tasks created), $RuleCand$ ($Rules$ before the \texttt{RemRedun} task) and $Rules$ (the final non-redundant rules).

These figures show that the total execution time of \textit{pNR$^3$} on 2 (8) threads has been reduced to 48-53\% (22-26\%) of that of \textit{NR$^3$}, respectively, while the runtime of the \texttt{GenPre} task takes about 20\% of \textit{NR$^3$}, and that of the \texttt{RemRedun} is negligible.
These results show that \textit{pNR$^3$} is faster than not only \textit{NR$^3$} but also other individual parallelizations, as we discussed in the previous section.
We can say that our algorithm shows good performance when we take into consideration the overhead of creating thousands of threads as shown in (c) and the fact that the algorithms have been executed in 4 physical cores.

We plot some results in Figure 2 (d)-(f) when varying $min\_conf$ from 50\% to 90\% with fixed $min\_sup = 0.5\%$ and $min\_sup^{all} = 1$ while the layout is the same as that of (a)-(c).
In Figure 2 (d)-(f), we notice the same behaviors as those in Figure 2 (a)-(c), thus we can say that our algorithm shows good performance in the synthetic database.

\begin{figure}[!ht]
	% \vspace{-20pt}
	\centering
	\caption{Results of D10C10N10R0.5; (a)-(c) varying $min\_ sup$ at $min\_ conf = 50\%$, (d)-(f) varying $min\_ conf$ at $min\_ sup = 0.5\%$}
	\includegraphics[width=12.2cm]{D10C10N10R05.pdf}
	\vspace{-20pt}
\end{figure}

\subsubsection{BMSWebView1}
We present experiment results of the BMSWebView1 dataset in Figure 3 with the same layout of Figure 2.

We plot some results in Figure 3 (a)-(c) when varying $min\_sup$ from 0.080\% to 0.100\% at $min\_conf=50\%$ and $min\_sup^{all} = 1$.
These figures show that the runtime of \textit{pNR$^3$} on 2 (8) threads has been reduced to about $1/2$ ($1/5$) of that of \textit{NR$^3$}, respectively, while the runtime of \texttt{GenRule} tasks takes almost 100\% in \textit{NR$^3$}.
These results show that \textit{pNR$^3$} is effectively faster than \textit{NR$^3$}.
But the effect of the task concurrency in \textit{pNR$^3$} is limited in this dataset, since the runtime of the \texttt{GenPre} task is small, as our discussion on the worst cases in the previous section.
Nevertheless, it is worthwhile to use \textit{pNR$^3$}, because \textit{pNR$^3$} has effectively reduced the total execution time, compared to the individual parallelizations.

We plot some results in Figure 3 (d)-(f) when varying $min\_conf$ from 50\% to 90\% with fixed $min\_sup = 0.090\%$ and $min\_sup^{all} = 1$.
In Figure 3 (d)-(f), we notice almost same behaviors as those in Figure 3 (a)-(c), except the ratio of the runtime of the \texttt{GenPre} task increases to 10\% as $min\_conf$ increases.

\begin{figure}[!ht]
	\vspace{-20pt}
	\centering
	\caption{Results of BMSWebView1; (a)-(c) varying $min\_ sup$ at $min\_ conf = 50\%$, (d)-(f) varying $min\_ conf$ at $min\_ sup = 0.090\%$}
	\includegraphics[width=12.2cm]{BMSWebView1.pdf}
	\vspace{-20pt}
\end{figure}

\section{Concluding Remarks}
We have proposed a parallel method for mining non-redundant recurrent rules from a sequence database, which is based on \textit{NR$^3$}, an existing sequential algorithm \cite{lo2008efficient}.
Our approach, \textit{pNR$^3$}, has fully exploited the task-parallelism in \textit{NR$^3$}, such as the loop-level parallelism and the task concurrency.
We have experimented \textit{pNR$^3$} on the synthetic and real datasets, and the performance results have shown the effectiveness of our algorithm in reducing the total execution time of \textit{NR$^3$}.

In the future, we will perform more experiments on different datasets including some program traces.
We will also try finer-level of parallelizations to fully utilize the effect of parallelization on large databases, as well as parallelization of \textit{BOB} \cite{lo2011bidirectional}.
Moreover, we will consider possible improvements of the sequential algorithms such as \textit{NR$^3$} and \textit{BOB}.
\vspace{-5pt}
\subsubsection{Acknowledgements}
This work was partially supported by JSPS Grant-in-Aid for Scientific Research (C) 15K00305.

% ---- Bibliography ----
%
\bibliographystyle{splncs03}
\bibliography{para-nr3}

\end{document}
